import cherrypy
from vispa.server import AbstractExtension
from vispa.controller import AbstractController
import logging
import os

logger = logging.getLogger(__name__)

class PXLBrowserExtension(AbstractExtension):

    def name(self):
        return "pxlbrowser"

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(PXLBrowserController())
        self.add_workspace_directoy()


class PXLBrowserController(AbstractController):
    def getrpc(self):
        windowId = cherrypy.request.private_params.get("_windowId", None)
        viewId = cherrypy.request.private_params.get("_viewId", None)
        return self.get("proxy", "PXLBrowserRpc", self.get("combined_id"),
                        window_id=windowId, view_id=viewId)

    @cherrypy.expose
    def start_reader(self, filepath):
        self.release_session()
        rpc = self.getrpc()
        rpc.start_reader(filepath)

    @cherrypy.expose
    def getOverview(self):
        self.release_session()
        rpc = self.getrpc()
        rpc.getOverview()

    @cherrypy.expose
    def getTree(self):
        self.release_session()
        rpc = self.getrpc()
        rpc.getTree()

    @cherrypy.expose
    def getFeynman(self):
        self.release_session()
        rpc = self.getrpc()
        rpc.getFeynman()
   
    @cherrypy.expose
    def getObjectProperties(self, oid):
        self.release_session()
        rpc = self.getrpc()
        rpc.getObjectProperties(oid)
    
    @cherrypy.expose
    def goto(self, index, indexed_objects, forbidden_strings):
        self.release_session()
        rpc = self.getrpc()
        rpc.goto(index, indexed_objects, forbidden_strings)

    @cherrypy.expose
    def setFilter(self, indexed_objects, forbidden_strings):
        self.release_session()
        rpc = self.getrpc()
        rpc.setFilter(indexed_objects, forbidden_strings)

    @cherrypy.expose
    def numberOfEvents(self):
        self.release_session()
        rpc = self.getrpc() 
        rpc.numberOfEvents()

    @cherrypy.expose
    def close(self):
        self.release_session()
        rpc = self.getrpc()
        rpc.close()
        self.extension.clear_workspace_instance(
            "PXLBrowserRpc",
            self.get("combined_id"))
