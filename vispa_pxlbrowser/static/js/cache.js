define(["jquery", "emitter"], function($, Emitter) {

  var Cache = Emitter._extend({
    init: function init(instance) {
      init._super.call(this);

      this.instance = instance;

      this.filepath = null;

      this.cache = {};
      this.memory = []; //memory to store the keys of the stored elements
      //to keep track of the cache size
    },

    initCache: function(callback) {
      var self = this;
      this.filepath = this.instance.state.get("path");

      //any incoming information should be saved in the cache
      this.instance.socket.on("getObjectProperties", function(data) {
        if (data.oid == self.instance.state.get("selectedID")) //only append if it matches selected state
          self.append(data.oid, data.properties, true);
      });
      this.instance.socket.on("getFeynman", function(data) {
        self.append("feynman", data, false);
      });
      this.instance.socket.on("getTree", function(data) {
        self.append("tree", data, false);
      });
      this.instance.socket.on("getOverview", function(data) {
        self.append("overview", data["overview"], false);
        self.append("overviewNames", data["names"], false);
      });

      this.instance.POST("start_reader", {
        filepath: self.filepath
      }, callback);
    },

    clearCache: function(key) {
      if (key === undefined) {
        this.cache = {};
        this.memory = [];
      } else {
        delete this.cache[key]
        this.memory.splice(this.memory.indexOf(key), 1);
      }
      return this;
    },

    getEntry: function(key, cb) {
      var self = this;
      if (this.cache[key])
        return {
          available: true,
          content: this.cache[key]
        };
      else {
        var f = null;
        var params = null;
        switch (key) {
          case "overview":
          case "overviewNames":
            f = "getOverview";
            break;
          case "feynman":
            f = "getFeynman";
            break;
          case "tree":
            f = "getTree";
            break;
          default:
            f = "getObjectProperties";
            params = {
              oid: key
            };
            break;
        }
        if (params)
          dfd = self.instance.GET(f, params, cb);
        else
          dfd = self.instance.GET(f, cb);
        return {
          available: false
        };
      }
    },

    append: function(key, entry, deletable) {
      this.cache[key] = entry;
      if (deletable)
        this.memory.push(key);

      if (this.memory.length > 10) { //limit the cache to 10 deletable objects
        var remIndex = this.memory.shift();
        delete this.cache[remIndex];
      }
      return this;
    }


  });

  return Cache;
});
