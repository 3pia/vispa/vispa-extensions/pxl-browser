define(["jquery", "emitter", "css!../css/windows"], function($, Emitter) {

  var Window = Emitter._extend({

    init: function(manager, name, view) {
      this.manager = manager;
      this.name = name;
      this.view = view;
      this.windowNode = null;

      this.isVisible = true;
      this.oldCSS = {
        top: undefined,
        height: undefined,
        left: undefined,
        width: undefined
      };
      this.isDblClicked = false;
      this.headerName = name.charAt(0).toUpperCase() + name.slice(1);
    },

    fixRelativeDimensions: function() {
      if (!this.windowNode)
        return;
      this.isDblClicked = false;
      var l = 100.0 * Math.max(0, this.windowNode.position().left / this.windowNode.parent().width());
      var t = 100.0 * Math.max(0, this.windowNode.position().top / this.windowNode.parent().height());
      var w = 100.0 * Math.min(0.99, this.windowNode.width() / this.windowNode.parent().width());
      var h = 100.0 * Math.min(0.99, this.windowNode.height() / this.windowNode.parent().height());
      
      this.manager.swapZIndex(this.name, t, h, l, w);
    },

    giveNode: function() {
      return this.windowNode;
    },

    handleDblClick: function() {
      this.isDblClicked = !this.isDblClicked;
      if (this.isDblClicked)
        this.setCSS(0.5, 99, 0.5, 99, undefined);
      else
        this.setCSS(this.oldCSS.top, this.oldCSS.height, this.oldCSS.left, this.oldCSS.width, undefined);
    },

    setContent: function(divContent) {
      this.windowNode.remove(".windowcontent");
      this.windowNode.append(divContent);
    },

    setup: function(top, height, left, width, zIndex, visible) {
      var self = this;
      visible = visible === undefined ? true : visible;

      this.windowNode = $("<div />").attr("id", this.name).addClass("panel panel-primary")
        .css("position", "absolute")
        .draggable({
          containment: "parent",
          cursor: "move",
          handle: ".panel-heading",
          snap: true,
          stop: function(event, ui) {
            self.fixRelativeDimensions(); //to be improved
          }
        }).resizable({
          ghost: true,
          minHeight: 100,
          minWidth: 150,
          handles: 'n,e,s,w,se',
          stop: function(event, ui) {
            self.fixRelativeDimensions(); //to be improved
          }
        }).on("mousedown", function() {
          self.manager.swapZIndex(self.name, undefined, undefined, undefined, undefined);
        });
      //append the header
      var windowHeader = $("<div />").addClass("panel-heading").append(
          $("<i />").addClass("glyphicon glyphicon-remove").css("cursor", "pointer")
          .click(function() {
            self.toggleVisibility();
          }))
        .on("dblclick", function() {
          self.handleDblClick();
        })
        .append($("<span />").addClass("windowheadercontent").html(this.headerName));

      this.windowNode.append(windowHeader);
      this.setCSS(top, height, left, width, zIndex);
      if (!visible)
        //Since the button toggling is somewhat hacked (a function to manipulate the tab buttons does not exists, yet)
        //we must ensure that the buttons are already attached to the DOM when "toggleVisibility" is called.
        //This explains the 50ms timeout before correctly setting the button icons during initialization.
        setTimeout(function() {
          self.toggleVisibility()
        }, 50);
    },

    setCSS: function(top, height, left, width, zIndex) {
      if (top !== undefined) {
        this.oldCSS.top = this.windowNode.css("top");
        this.windowNode.css("top", typeof(top) == "string" ? top : String(top) + "%");
      }
      if (height !== undefined) {
        this.oldCSS.height = this.windowNode.css("height");
        this.windowNode.css("height", typeof(height) == "string" ? height : String(height) + "%");
      }
      if (left !== undefined) {
        this.oldCSS.left = this.windowNode.css("left");
        this.windowNode.css("left", typeof(left) == "string" ? left : String(left) + "%");
      }
      if (width !== undefined) {
        this.oldCSS.width = this.windowNode.css("width");
        this.windowNode.css("width", typeof(width) == "string" ? width : String(width) + "%");
      }
      if (zIndex !== undefined) {
        this.windowNode.css("z-index", zIndex);
      }
      if (top || height || left || width)
        this.view.afterResize();
    },


    toggleVisibility: function() {
      this.isVisible = !this.isVisible;
      if (this.isVisible) {
        this.windowNode.css("visibility", "");
        this.manager.swapZIndex(this.name, undefined, undefined, undefined, undefined);
      } else
        this.windowNode.css("visibility", "hidden");
      this.manager.instance.toggleButtonIcon(this.name, this.isVisible);
    },

    //returns CSS attributes as strings
    getRelativeDimensions: function() {
      return {
        t: 100.0 * Math.max(0, this.windowNode.position().top / this.windowNode.parent().height()),
        h: 100.0 * Math.min(0.99, this.windowNode.height() / this.windowNode.parent().height()),
        l: 100.0 * Math.max(0, this.windowNode.position().left / this.windowNode.parent().width()),
        w: 100.0 * Math.min(0.99, this.windowNode.width() / this.windowNode.parent().width()),
        z: this.windowNode.css("z-index")
      }
    },
    //returns real pixel values as floats (e.g. for repositioning during the session)
    getDimensions: function() {
      return {
        w: this.windowNode.width(),
        h: this.windowNode.height()
      }
    }

  });

  return Window;
});