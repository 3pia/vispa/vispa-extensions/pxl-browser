define(["jquery", "emitter", "css!../css/sidebar"], function($, Emitter) {


  var Sidebar = Emitter._extend({
    init: function(instance) {
      var self = this;
      this.instance = instance;

      this.cuts = {};
      this.wrapperCuts = null;

      this.wrapperSchemes = null;
      this.schemeCounter = 0;
    },

    setup: function(node) {
      var self = this;
      // get Sidebar
      this.$sidebarNode = node.find(".control-sidebar");
      // get Sidebar toggle Button
      this.$sidebarToggleButton = this.$sidebarNode.find(".toggle-button ul");
      this.$toggleIcon = this.$sidebarToggleButton.find("li i");
      this.$iconPopover = this.$sidebarNode.find(".item-icon");
      this.$sidebarDetailsTrigger = this.$sidebarNode.find(".trigger");
      // toggle sidebar
      this.$sidebarToggleButton.click(function() {
        // small/large sidebar
        self.$sidebarNode.toggleClass("sidebar-small");
        self.$sidebarNode.toggleClass("sidebar-large");
        // set icon for toggle button
        self.$toggleIcon.toggleClass("fa-angle-left");
        self.$toggleIcon.toggleClass("fa-angle-right");
        // toggle Sidebar popover
        self.toggleSidebarPopover();
        self.toggleSidebarDetails();
      });

      // toggle Sidebar popover
      this.toggleSidebarPopover();
      this.toggleSidebarDetails();

      // make cuts work
      this.setupCuts();

      //colour schemes
      this.setupSchemes();
    },

    toggleSidebarPopover: function() {
      // popover on icon when sidebar is small
      var self = this;

      if (this.$sidebarNode.hasClass("sidebar-small")) {
        this.$iconPopover.each(function() {
          ($(this).parent().parent()).toggleClass("details-hide", true);
          ($(this).parent().parent()).toggleClass("details-show", false);
          ($(this).parent().parent()).toggleClass("active", false);
          ($(this).parent().find(".toggle-down i")).toggleClass("fa-angle-down", true);
          ($(this).parent().find(".toggle-down i")).toggleClass("fa-angle-up", false);
          $(this).popover();
        });
      } else {
        this.$iconPopover.each(function() {
          $(this).popover('destroy');
        });
      }
    },

    toggleSidebarDetails: function() {
      var self = this;
      // if sidebar is large
      if (!this.$sidebarNode.hasClass("sidebar-small")) {
        this.$sidebarDetailsTrigger.each(function() {
          //unbind the functionality first
          $(this).unbind();
          $(this).click(function() {
            ($(this).find(".toggle-down i")).toggleClass("fa-angle-up");
            ($(this).find(".toggle-down i")).toggleClass("fa-angle-down");
            ($(this).parent()).toggleClass("details-hide");
            ($(this).parent()).toggleClass("details-show");
            ($(this).parent()).toggleClass("active");
          })
        })
      }
    },

    setupCuts: function() {
      var self = this;
      this.wrapperCuts = this.$sidebarNode.find(".wrapper-cuts");

      this.cutEnableDiv = $("<div/>").addClass("checkbox")
        .append(
          $("<label/>").append(
            $("<input/>").attr({
              "type": "checkbox"
            }).click(function() {
              self.updateViews();
            })
          ).append("Enable cuts")
        );
      this.wrapperCuts.append(this.cutEnableDiv);


      this.addCut("pt", 0, 4000, 1.0, "logarithmic", "GeV");
      this.addCut("E", 0, 4000, 1.0, "logarithmic", "GeV");
      this.addCut("Eta", -5, 5, 0.1, "", "");

    },

    setupSchemes: function() {
      var self = this;
      this.wrapperSchemes = this.$sidebarNode.find(".wrapper-colors");
      //key must match the one given in "Helpers.assignColor()"
      this.addScheme("Geant 4 Colours", "Geant4", true);
      //use default colour only
      this.addScheme("None", "None", false);
    },

    addCut: function(type, min, max, step, scale, unit) {
      var self = this;

      //register the cut
      this.cuts[type] = {
        "min": min,
        "initialMin": min,
        "max": max,
        "initialMax": max
      };

      min = String(min);
      max = String(max);

      //make the html
      var wrapperSlider = $("<div/>").addClass("wrapper-slider")
        .append(
          $("<div/>").addClass("label-slider")
          .html(type)
        )
        .append(min)
        .append(
          $("<input/>").attr({
            "id": "sliderCut" + type,
            "type": "text",
            "value": "",
            "data-slider-min": min,
            "data-slider-max": max,
            "data-slider-step": String(step),
            "data-slider-value": "[" + min + "," + max + "]"
          })

        )
        .append(max + " " + unit);
      this.wrapperCuts.append(wrapperSlider);

      //init as a slider
      wrapperSlider = this.wrapperCuts.find("#sliderCut" + type);
      var sliderOpts = {
        tooltip: "always"
      };
      if (scale)
        sliderOpts["scale"] = scale;
      wrapperSlider.slider(sliderOpts);

      wrapperSlider.on("slideStop", function() {
        var newThresholdMin = wrapperSlider.slider("getValue")[0];
        var newThresholdMax = wrapperSlider.slider("getValue")[1];
        self.cuts[type].min = newThresholdMin;
        self.cuts[type].max = newThresholdMax;
        self.updateViews();
      });
    },

    addScheme: function(name, key, checked) {
      this.schemeCounter += 1;
      var nr = String(this.schemeCounter);
      var ID = "optionsRadios" + nr;
      var radioDiv = $("<div/>").addClass("radio")
        .append(
          $("<label/>")
          .append(
            $("<input/>").attr({
              "type": "radio",
              "name": "optionsRadios",
              "id": ID,
              "value": key
            })
          ).append(name)
        );
      if (checked)
        radioDiv.find("input").attr("checked", checked);
      this.wrapperSchemes.append(radioDiv);
    },

    isGood: function(obj) {
      var isGood = true
      var veto = !this.cutEnableDiv.find("input").prop("checked");
      for (var key in this.cuts) {
        var cutMin = this.cuts[key].min;
        var cutMax = this.cuts[key].max;
        var cutInitialMin = this.cuts[key].initialMin;
        var cutInitialMax = this.cuts[key].initialMax;

        var is = obj[key];
        var stillGood = true;
        if (typeof(is) == "number")
          stillGood = ((is >= cutMin) && (is <= cutMax));
        else if (is == "+inf") {
          stillGood = (this.cuts[key].initialMax == cutMax);
        } else if (is == "-inf")
          stillGood = (this.cuts[key].initialMin == cutMin);
        isGood = isGood && stillGood;
      }

      return isGood || veto;
    },

    getScheme: function() {
      return this.wrapperSchemes.find("input").filter(":checked").val();
    },

    updateViews: function() {
      var relations = this.instance.viewManager.giveViewContent("relations");
      if (relations)
        relations.update();
      var feynman = this.instance.viewManager.giveViewContent("feynman");
      if (feynman)
        feynman.update();
    }
  })

  return Sidebar;

});
