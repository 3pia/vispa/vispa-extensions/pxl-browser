define(["jquery", "emitter", "./views/overview", "./views/properties", "./views/relations", "./views/feynman",
    "./views/view3d"
  ],
  function($, Emitter, Overview, Properties, Relations, Feynman, View3D) {

    var ViewManager = Emitter._extend({

      init: function(instance) {
        var self = this;

        this.instance = instance;
        //the node that contains all windows
        this.viewNode = null;

        //the label
        this.infolabelBox = null;
        this.labelContent = null;
        // this.labelLabel = null;
        //index keeping & views

        this.index = this.instance.state.get("index");
        this.maxIndex = this.instance.state.get("maxIndex");
        this.maxIndexObtained = this.instance.state.get("maxIndexObtained");

        this.instance.on("changedState", function(key) {
          switch (key) {
            case "index":
              self.index = this.state.get("index");
              break;
            case "maxIndex":
              self.maxIndex = this.state.get("maxIndex");
              break;
            case "maxIndexObtained":
              self.maxIndexObtained = this.state.get("maxIndexObtained");
              break;
          }
        });

        //some websocket listeners:
        this.instance.socket.on("goto", function(data) {
          if (data.success) {
            self.instance.state.set("index", data.newIndex);
            self.instance.cache.clearCache();
            self.generateViews();
          }
          self.instance.loading = false;
        });
        this.instance.socket.on("numberOfEvents", function(data) {
          self.instance.state.set("maxIndex", data.NoF);
          self.instance.state.set("maxIndexObtained", !data.estimate);
          self.updateInfoLabel();
        });

        //central object containing all views
        this.views = [];
        /*uncomment when #2216 is fulfilled
        //control buttons
        this.toggleButtons = [];
        */
      },

      generateViews: function() {
        for (var i = 0; i < this.views.length; i++)
          this.views[i].update();

        this.updateInfoLabel();
      },

      giveViewContent: function(name) {
        for (var i = 0; i < this.views.length; i++) {
          if (this.views[i].name == name)
            return this.views[i];
        }
        return null;
      },

      goToPrompt: function() {
        var self = this;
        this.instance.prompt("go to event # ", function(index) {
          if (index === null) //if prompt is closed
            return;
          index = parseInt(index);
          if (!index) {
            self.instance.alert("Not a valid index!");
            return;
          }
          self.goTo(index);
        });
      },

      goTo: function(index) {
        var self = this;
        //if no index is indicated ->
        index = index === undefined ? this.instance.state.get("index") : index;

        if (index < 1 || !this.instance.state.get("path")) //if no path is indicated -->
        //no file is open --> nothing to scroll
          return;
        if (index > this.maxIndex && this.maxIndexObtained) {
          this.instance.alert("Index exceeds number of events.");
          return;
        }
        this.instance.loading = true;
        //here we tell the reader to simply go to the next event
        //response is provided by websockets, callback is defined up top
        this.instance.POST("goto", {
          index: index,
          indexed_objects: this.instance.prefs.get("ShowIndexedObjects"),
          forbidden_strings: this.instance.prefs.get("HideObjects")
        });
      },

      //function to store the last window settings
      memorizeWindowPlacements: function() {
        var settings = {};
        for (var i = 0; i < this.views.length; i++) {
          settings[this.views[i].name] = {
            "isVisible": this.views[i].window.isVisible,
            "css": this.views[i].window.getRelativeDimensions()
          }
        }
        $.cookie("pxlbrowser_window_settings", JSON.stringify(settings), {
          expires: 7
        });
      },
      //convenience navigation functions
      nextEvent: function() {
        this.goTo(this.index + 1);
      },
      prevEvent: function() {
        this.goTo(this.index - 1);
      },

      registerView: function(name, view) {
        var self = this;

        //check if id name is already taken
        for (var i = 0; i < this.views.length; i++) {
          if (this.views[i].name == name)
            return;
        }

        this.views.push(view.setup(name));

        /*uncomment when #2216 is fulfilled
        this.toggleButtons.push({
          label: name.charAt(0).toUpperCase() + name.slice(1),
          id: name,
          iconClass: "glyphicon glyphicon-ok",
          buttonClass: "btn-default",
          callback: function() {
            view.window.toggleVisibility();
          }
        });
        */
      },

      setup: function(node) {
        var self = this;

        this.viewNode = $(node).find(".views");
        this.defaultContent = this.viewNode.html();

        this.infolabelBox = node.find(".infolabel");
        this.labelContent = this.infolabelBox.find(".content");
        //this.labelLabel = node.find(".infolabel .label");

        //register the different views here
        //naming must be like the names in the checkboxes

        //register the three main views:
        this.registerView("overview", new Overview(this));
        this.registerView("properties", new Properties(this));


        //register the optional views
        if (this.instance.prefs.get("Relations"))
          this.registerView("relations", new Relations(this));
        if (this.instance.prefs.get("Feynman"))
          this.registerView("feynman", new Feynman(this));

        /*uncomment when #2216 is fulfilled
        //place the toggle buttons dynamically
        this.instance.addMenuEntry("view", {
          label: "View",
          iconClass: "glyphicon glyphicon-th-large",
          buttonClass: "btn-default",
          childrenStyle: "group",
          children: this.toggleButtons
        });
        */

        //place the windows without content
        this.placeWindows(true);
        this.setupInfoLabel();
      },

      swapZIndex: function(name, t, h, l, w) {
        for (var i = 0; i < this.views.length; i++) {
          if (this.views[i].name == name)
            this.views[i].window.setCSS(t, h, l, w, 1);
          else
            this.views[i].window.setCSS(undefined, undefined, undefined, undefined, 0);
        }
      },

      placeWindows: function(init) {
        var cookie = $.cookie("pxlbrowser_window_settings");
        if (cookie)
          cookie = JSON.parse(cookie);

        var N = 0;
        for (var i = 0; i < this.views.length; i++) {
          if (this.views[i].window.isVisible) //default @init: all windows are visible
            N += 1;
        }
        if (N == 0) {
          console.log("No window to be placed.");
          return;
        }
        var placementSettings = initialPlacementSettings(N);
        var j = 0;
        for (var i = 0; i < this.views.length; i++) {
          var window = this.views[i].window;
          var name = this.views[i].name;
          window.isDblClicked = false; //reset the double click status
          if (!window.isVisible)
            continue;
          var t = null,
            h = null,
            l = null,
            w = null,
            z = null;
          var isVisible = true;
          if (cookie && init && cookie[name]) {
            t = cookie[name].css.t;
            h = cookie[name].css.h;
            l = cookie[name].css.l;
            w = cookie[name].css.w;
            z = cookie[name].css.z;
            isVisible = cookie[name].isVisible;
          } else {
            t = placementSettings.top[j];
            h = placementSettings.height[j];
            l = placementSettings.left[j];
            w = placementSettings.width[j];
            z = 0;
          }


          j += 1;
          if (init) {
            window.setup(t, h, l, w, z, isVisible);
            this.viewNode.append(window.giveNode());
          } else
            window.setCSS(t, h, l, w, undefined);
        }
      },

      setupInfoLabel: function() {
        var self = this;
        // make icons clickable to navigate through events
        var $firstEvent = this.infolabelBox.find(".fa-step-backward");
        var $previousEvent = this.infolabelBox.find(".fa-chevron-left");
        var $followingEvent = this.infolabelBox.find(".fa-chevron-right");
        var $lastEvent = this.infolabelBox.find(".fa-step-forward");

        $firstEvent.click(function() {
          self.goTo(1);
        });
        $previousEvent.click(function() {
          self.prevEvent();
        });
        $followingEvent.click(function() {
          self.nextEvent();
        });
        $lastEvent.click(function() {
          self.goTo(self.maxIndex);
        });

        this.labelContent.click(function() {
          self.goToPrompt();
        });
      },

      updateInfoLabel: function() {
        var self = this;
        this.labelContent.html(String(this.index) +
          "/" + (this.maxIndexObtained == true ? "" : ">=") +String(this.maxIndex));
      }
    });



    var initialPlacementSettings = function(N) {
      switch (N) {
        case 1:
          return {
            top: [0.5],
            height: [99],
            left: [0.5],
            width: [99]
          }
          break;
        case 2:
          return {
            top: [0.5, 0.5],
            height: [99, 99],
            left: [0.5, 50.5],
            width: [49, 49]
          }
          break;
        case 3:
          return {
            top: [0.5, 0.5, 0.5],
            height: [99, 99, 99],
            left: [0.5, 33.5, 66.5],
            width: [33, 33, 33]
          }
          break;
        case 4:
          return {
            top: [0.5, 0.5, 50.5, 50.5],
            height: [49, 49, 49, 49],
            left: [0.5, 50.5, 0.5, 50.5],
            width: [49, 49, 49, 49]
          }
          break;
        case 5:
          return {
            top: [0.5, 0.5, 0.5, 50.5, 50.5],
            height: [49, 49, 49, 49, 49],
            left: [0.5, 33.5, 66.5, 16.5, 60.5],
            width: [33, 33, 33, 33, 33]
          }
          break;
        case 6:
          return {
            top: [0.5, 0.5, 0.5, 50.5, 50.5, 50.5],
            height: [49, 49, 49, 49, 49, 49],
            left: [0.5, 33.5, 66.5, 0.5, 33.5, 66.5],
            width: [33, 33, 33, 33, 33, 33]
          }
          break;
        default:
          alert("Uncaught exception concerning number of views! Please contact the VISPA team for support.");
      }
    };

    return ViewManager;
  });
