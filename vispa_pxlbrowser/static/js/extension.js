define([
  "vispa/extension",
  "vispa/filehandler2",
  "./view",
], function(
  Extension,
  FileHandler2,
  PXLBrowserView
) {
  var PXLBrowserExtension = Extension._extend({
    init: function init() {
      init._super.call(this, "pxlbrowser", "PXL Browser");
      this.mainMenuAdd([
        this.addView(PXLBrowserView),
      ]);
    }
  });

  FileHandler2.addOpen("pxlbrowser", {
    label: "PxlIO Browser",
    iconClass: "fi-pxlio",
    position: 1,
  }, function (path) {
    return ["pxlbrowser", "PXLBrowserView", {path: path}];
  }, ["pxlio"]);

  return PXLBrowserExtension;
});
