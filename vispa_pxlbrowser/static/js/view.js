define([
  "jquery",
  "vispa/views/main",
  "./prefs",
  "./view_manager",
  "./cache",
  "./sidebar",
  "text!../html/main.html",
  "css!../css/style"
], function(
  $,
  MainView,
  Prefs,
  ViewManager,
  Cache,
  Sidebar,
  MainTemplate
) {

    var PXLBrowserView = MainView._extend({

      init: function init(args) {
        init._super.apply(this, arguments);

        //toggle buttons are initialized here (for now, until appropriate feature #2216 request has been implememted)
        ["Relations","Feynman"].forEach(function(name){
          if(this.prefs.get(name))
            this.menu.del(name.toLowerCase());
        }.bind(this));

        this.setupState({
          path: undefined,
          index: 1,
          maxIndex: undefined,
          maxIndexObtained: false,
          selectedID: null
        }, args);

        this.node = null;
        this.viewManager = new ViewManager(this);
        this.Sidebar = new Sidebar(this);
        this.cache = new Cache(this);

        this.socket.on("watch", function(data) {
          if (data.watch_id != "pxl")
            return;
          if (data.event == "vanish") {
            self.confirm("File has been deleted or renamed. \n Please open a new file or" +
              "the browser is closed.", function(res) {
                self.close();
            });
          }
        });
      },

      getFragment: function() {
        return this.state.get("path") || "";
      },

      applyFragment: function(fragment) {
        this.state.set("path", fragment);
        return this;
      },

      applyPreferences: function applyPreferences() {
        applyPreferences._super.call(this);

        if (this.viewManager.views.properties !== undefined)
          this.viewManager.views.properties.content.formatRows();
      },

      //initial setup
      render: function(node) {
        var self = this;
        this.loading = true;
        this.node = node;

        //get the main html content
        $(MainTemplate).appendTo(node);
        this.viewManager.setup(node);
        this.Sidebar.setup(node);
        this.loading = false;

        this.setLabel(this.state.get("path"), true);
        //setup file watch
        this.POST("/ajax/fs/watch", {
          path: this.state.get("path"),
          watch_id: "pxl"
        });
        this.cache.initCache(function(err) {
          if (err)
            console.log("Something went wrong during cache initializiation.");
          //ask for the number of events; the view_manager then listens to the event
          this.POST("numberOfEvents", function(){
            self.viewManager.goTo(1);
          })
        }.bind(this));
      },

      toggleButtonIcon: function(buttonID, visible) {
        this.menu.get("view").get(buttonID).iconClass = visible ? "fa-check" : "fa-x";
      },

      onBeforeClose: function() {
        this.POST("/ajax/fs/unwatch", {});
        this.cache.clearCache();
        this.viewManager.memorizeWindowPlacements();
        this.POST("close");
      },

      openNew: function() {
        vispa.mainMenu.get("extension_pxlbrowser_item0").runCallback();
      },
    },{
      maxInstances: 1, // why??
      iconClass: "fa-picture-o",
      label: "PXL Browser",
      name: "PXLBrowserView",
      menuPosition: 121,
      menuFFcallback: function(path) {
        if (!path) return;
        if (path.split(".").pop().toLowerCase() !== "pxlio") {
          vispa.messenger.alert("The selected file is not a valid PXLio file!");
        } else {
          return {path: path};
        }
      },
      preferences: {
        items: Prefs.prefs,
      },
      menu: Prefs.menu,
      fastmenu: ["goto", "prev", "next", "view", "align"],
      fileHandlers:  [{
        label: "PXL Browser",
        iconClass: "fa-picture-o",
        position: 1,
        hidden: function () {
          var ext = this.value.split(".").pop().toLowerCase();
          return ext !== "pxlio";
        },
        callback: function () {
          (this.$root && this.$root.view || vispa).spawnInstance("pxlbrowser", "PXLBrowserView",
            {path: this.value});
        }
      }],
    });

    return PXLBrowserView;
  });
