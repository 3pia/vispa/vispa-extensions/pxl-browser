define(function() {

  //object contaning the object to color matching
  var assignColor = function(scheme, name) {
    var colorMap = {};

    if (scheme == "Geant4")
      colorMap = { //to be extended or generalized
        //red: negative fermions
        "rgba(255,0,0,": [
          //leptons
          "e", "e-", "mu", "mu-", "tau", "tau-",
          //quarks
          "u", "dbar", "c", "sbar", "b", "tbar"
        ],
        //grey: neutrinos
        "rgba(0,255,0,": [
          "nu_e", "nu_ebar", "nu_mu", "nu_mubar", "nu_tau", "nu_taubar"
        ],
        //blue: positve fermions
        "rgba(0,0,255,": [
          //leptons
          "e+", "mu+", "tau+",
          //quarks
          "ubar", "d", "cbar", "s", "bbar", "t"
        ],
        //dark grey: exchange bosons
        "rgba(50,50,50,": ["Z", "Z0", "g", "string", "gamma", "h0", "h", "W+", "W-", "W"],
        //dark blue: positive hadrons
        "rgba(0,0,128,": ["p+", "B*+", "B+", "pi+", "rho+", "K*+", "K+"],
        //dark red: negative hadrons
        "rgba(128,0,0,": ["p-", "B*-", "B-", "pi-", "rho-", "K*-", "K-", "Sigma-", "omega"],
        //dark green: neutral hadrons
        "rgba(0,128,0,": ["B*bar0", "B*0", "pi0", "K_L0", "K_S0", "Delta0", "rho0", "eta", "n0", "nbar0"]
      };

    for (var k in colorMap) {
      if (colorMap[k].indexOf(name) != -1)
        return k;
    }
    return "rgba(102,102,102,"; //default color if not specified
  };

  var assignEdgeType = function(name, curve) {
    var typeMap = {
      "dotted": ["h0", "h"],
      "dashed": ["Z", "Z0", "W+", "W-", "W"],
      "curly": ["string", "gamma"],
      "loop": ["g"]
    }
    for (var k in typeMap) {
      if (typeMap[k].indexOf(name) != -1)
        return k
    }
    return curve ? "curve" : "line";
  };

  //object containig the zoombar div
  var getControlBarNode = function() {
    return $("<div />").addClass("btn-group")
      .append(
        $("<a />").attr({
          id: "fastmenu-refresh",
          class: "btn btn-sm btn-default",
          role: "button",
          title: "Update/Rescale the graph to fit the window dimensions"
        }).append($("<i />")
          .addClass("glyphicon glyphicon-refresh")
        )
      ).append(
        $("<a />").attr({
          id: "fastmenu-out",
          class: "btn btn-sm btn-default",
          role: "button",
          title: "-20%"
        }).append($("<i />")
          .addClass("glyphicon glyphicon-zoom-out")
        )
      ).append(
        $("<a />").attr({
          id: "fastmenu-reset",
          class: "btn btn-sm btn-default",
          title: "100%",
          role: "button",
          title: "Reset zoom level to 100% without rescaling"
        }).append($("<i />")
          .addClass("glyphicon glyphicon-fullscreen")
        )
      ).append(
        $("<a />").attr({
          id: "fastmenu-in",
          class: "btn btn-sm btn-default",
          role: "button",
          title: "+25%"
        }).append($("<i />")
          .addClass("glyphicon glyphicon-zoom-in")
        )
      );
  };

  var reactToControl = function(self, action) {
    if (!self.s)
      return;

    switch (action) {
      case ("refresh"):
        {
          self.update();
          return;
        }
      case ("in"):
        {
          self.s.cameras[0].ratio = self.s.cameras[0].ratio / 1.2;
          break;
        }
      case ("out"):
        {
          self.s.cameras[0].ratio = self.s.cameras[0].ratio * 1.2;
          break;
        }
      case ("reset"):
        {
          self.s.cameras[0].ratio = 1.05;
          self.s.cameras[0].x = 0;
          self.s.cameras[0].y = 0;
          break;
        }
      case ("flip"):
        {
          self.isFlipped = !self.isFlipped;
          self.update();
          break;
        }
      default:
        return;
    }
    clearTimeout(self.refreshTimeOut);
    self.refreshTimeOut = null;
    //trigger the refresh after the stopping has stopped
    self.refreshTimeOut = setTimeout(function() {
      self.s.refresh();
    }, 50);
  };


  return {
    assignColor: assignColor,
    assignEdgeType: assignEdgeType,
    getControlBarNode: getControlBarNode,
    reactToControl: reactToControl,
  }
});