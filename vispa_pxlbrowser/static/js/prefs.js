define(["vispa/utils"], function(Utils) {
  var prefs = {
    ClickableEdges: {
      label: "clickable edges",
      type: "boolean",
      value: false,
      description: "Clickable edges in the Feynman view? \n  Attention: Requires a lot of RAM for large files!Check 'false' in those cases.",
    },
    Feynman: {
      label: "Feynman view",
      type: "boolean",
      value: true,
      description: "Load the 'Feynman' view? Attention: Requires a lot of RAM for large files! Check 'false' in those cases.",
    },
    Relations: {
      label: "Relations view",
      type: "boolean",
      value: true,
      description: "Load the 'Relation' view?"
    },
    ShowIndexedObjects: {
      label: "Show indexed objects",
      type: "string",
      value: "",
      description: "Show the indicated objects only if they are indexed. Strings are seperated by a ';'.",
    },
    HideObjects: {
      label: "Objects to hide",
      type: "string",
      value: "",
      description: "Do not display objects whose name match one of the strings. Strings are seperated by a ';'. '*' filters everything.",
    }
  };

  var toggleButtons = {};
  ["overview", "properties", "relations", "feynman" /*, "3dview" */ ].map(function(name) {
    toggleButtons[name] = {
      label: Utils.capitalize(name),
      iconClass: "fa-check",
      hidden: false,
      callback: function() {
        this.$root.instance.viewManager.giveViewContent(name).window.toggleVisibility();
      }
    };
  });

  var menu = {
    Align: {
      label: "Align",
      iconClass: "fa-align-justify",
      callback: function() {
        this.$root.instance.viewManager.placeWindows();
      },
    },
    openFile: {
      label: "Open file...",
      iconClass: "fa-file",
      callback: function() {
        this.$root.instance.openNew();
      },
    },
    goto: {
      label: "Go to",
      iconClass: "fa-play",
      callback: function() {
        this.$root.instance.viewManager.goToPrompt();
      },
    },
    ovw_filter: {
      label: "Overview filter",
      iconClass: "fa-filter",
      callback: function() {
        this.$root.instance.viewManager.giveViewContent("overview").showNames();
      },
    },
    view: {
      label: "View",
      iconClass: "fa-th-large",
      fastmenu: "group",
      items: toggleButtons,
    },
  };

  return {
    prefs: prefs,
    menu: menu,
  };
});
