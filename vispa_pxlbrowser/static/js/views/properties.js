define(["jquery", "emitter", "./../window", "css!../../css/views/properties"], function($, Emitter, Window) {

  var Properties = Emitter._extend({
    init: function(manager) {
      /*** mandatory***/
      this.instance = manager.instance;
      this.manager = manager;
      this.name = null;
      this.window = null;
      this.Node = null;
      /********/

      this.selectedNode = null;

      //need unique IDs for collapsing single tables
      this.timestamp = String((new Date()).getTime());
      this.currentlyVisible = ["Particle" + this.timestamp]; //table headers that are by default shown
    },
    /***mandatory function***/
    setup: function(name) {
      this.name = name;
      this.window = new Window(this.manager, name, this);
      var self = this;
      this.instance.on("changedState", function(key) {
        switch (key) {
          case "selectedID":
            var ID = self.instance.state.get("selectedID");
            if (ID)
              self.update();
            break;
        }
      });
      return this;
    },
    /***mandatory function***/
    reset: function() {
      if (this.Node)
        this.Node.empty();
      else
        this.Node = $("<div />").addClass("windowcontent");
      return this;
    },
    /***mandatory function***/
    update: function() {
      var self = this;

      this.reset();

      var objID = this.instance.state.get("selectedID");
      if (!objID) {
        this.window.setContent(this.Node);
        this.doWhenAppend();
        return;
      }

      var cb = function(err) {
        if (err) {
          console.log("Something went wrong while loading the properties.");
          return;
        } else
          self.update();
      }
      var res = this.manager.instance.cache.getEntry(objID, cb);
      if (res.available) {
        this.generateTable(res.content);
        this.window.setContent(this.Node);
        this.doWhenAppend();
      }
    },
    /***mandatory function***/
    doWhenAppend: function() {
      return this;
    },
    /***mandatory function***/
    afterResize: function() {},


    /***********************************************************************/
    /************View specific functions********************/
    /***********************************************************************/

    generateTable: function(data) {
      var self = this;
      var name = null;
      for (var key in data) {
        name = key;
      }
      data = data[name];
      var headerNode = this.makeHeader(name);
      this.Node.append(headerNode);
      
      this.Node.append($("<div/>")
        .addClass("tables")
        .css({
          height: "calc(100% - 25px)",
          width: "100%",
          overflow: "auto"
        })
      );
      $.each(data, function(i, entry) {
        //get the key of the specific entry
        var key = [];
        for (var k in entry)
          key.push(k);
        key = key[0];

        var values = entry[key]; //array containing all possible values
        var id = key;

        //create some divs
        var outer = $("<div />").addClass("gridpanel")
          .appendTo(self.Node.find(".tables"));
        $("<a />").addClass("gridpanel_label")
          .append($("<span />"))
          .append(key).attr({
            "href": "#" + id + self.timestamp,
            "data-toggle": "collapse"
          }).click(function() { //remember if table has been clicked (default: no --> not visible)
            $.each(outer.find(".collapse"), function() {
              var k = $(this).attr("id");
              var i = self.currentlyVisible.indexOf(k);
              if (i == -1)
                self.currentlyVisible.push(k);
              else
                self.currentlyVisible.splice(i, 1);
            });
          }).appendTo(outer);

        //check if value array exists
        if (!$.isArray(values) || !values.length)
          return;

        //here the tables are made
        var grid = self.makeTable(values, id + self.timestamp);
        $(grid).appendTo(outer);

      });

      //round all the numbers when done
      this.formatRows();
      //if previously no element has been selected, we are done at this point
      if (this.selectedNode === null)
        return;

      var selectedElement = this.getElementByID(this.selectedNode.id);
      this.selectElement(selectedElement);
      this.scrollToElement(selectedElement, 100);

    },

    makeHeader: function(name) {
      var overviewContent = this.manager.giveViewContent("overview")
      var headline = "";
      if (!overviewContent || !overviewContent.isSelected())
        headline = name;
      else {
        headline = overviewContent.selectedLabel();
        headline = headline + " #" + String(overviewContent.selectedRelPos());
      }

      var header = $("<div />")
        .addClass("data_header")
        .html(headline);
      return header;
    },

    makeTable: function(data, id) {
      var self = this;

      var tableDiv = $("<div />").addClass("collapse").attr("id", id);
      if (self.currentlyVisible.indexOf(id) != -1) {
        //show the table if visible by default or %1 times clicked
        tableDiv.addClass("in");
      }
      var table = $("<table />").addClass("table table-hover table-condensed")
        .appendTo(tableDiv);

      var head = $("<tr />").appendTo($("<thead />")).appendTo(table);

      head.append($("<th />").html("Property"));
      head.append($("<th />").html("Value"));

      var body = $("<tbody />").appendTo(table);
      $.each(data, function(i, entry) {
        var title = null;
        var label = null;
        var value = null;
        for (key in entry) {
          if (key == "doc")
            title = entry[key];
          else {
            value = entry[key];
            label = key;
          }
        }
        var row = $("<tr />").attr("id", label.replace(" ", "")) //no empty spaces in IDs
          .attr("data-content", String(value));
        row.append(
          $("<td />").attr("id", "key")
          .attr("title", title)
          .html(String(label))
        );

        var valueNode = $("<td />").attr("id", "value");
        if (isNumber(value)) {
          valueNode.html("");
        } else if (["Daughter ids", "Mother ids"].indexOf(label) != -1) {
          $.each(value, function(i, entry) {
            valueNode.append(
              $("<a />").append(
                $("<div />").html(String(entry)).click(function() {
                  self.instance.state.set("selectedID", entry);
                })
              )
            )
          })
        } else if (["Daughter names", "Mother names"].indexOf(label) != -1) {
          $.each(value, function(i, entry) {
            var content = entry.split("?ID=");
            valueNode.append(
              $("<a />").append(
                $("<div />").html(String(content[0])).click(function() {
                  self.instance.state.set("selectedID", content[1]);
                })
              )
            )
          })
        } else {
          valueNode.html(String(value));
        }
        row.append(valueNode);

        row.on({
          click: function() {
            if (["Daughterids", "Motherids"].indexOf($(this).attr("id")) == -1) {
              self.selectElement($(this));
            }
          },
          mouseover: function() {
            $("body").css("cursor", "pointer");
          },
          mouseleave: function() {
            $("body").css("cursor", "default");
          }
        });
        row.appendTo(body);
      });

      return tableDiv;
    },

    selectElement: function(el) { //toggles color and toggles decimals
      if (el.length === 0)
        return;
      var value = el.attr("data-content");
      el.css("background-color", "#FFFF66");
      //reset the rounding
      el.find("#value").html(value);

      if (this.selectedNode !== null && this.selectedNode.id != el.attr("id")) {
        if (isNumber(value)) {
          value = this.roundEntry(this.selectedNode.node.attr("data-content"));
          this.selectedNode.node.find("#value")
            .html(String(value));
        }
        this.selectedNode.node.css("background-color", "");
      }
      this.selectedNode = {
        node: el,
        id: el.attr("id")
      };

      return this;
    },

    getElementByID: function(id) {
      return this.Node.find("#" + id);
    },

    scrollToElement: function(el, delay) {
      if (el.length === 0)
        return;
      this.scrollTimeOut = setTimeout(function() {
        $(el).get(0).scrollIntoView();
      }, delay !== undefined ? delay : 1000);

      return this;
    },

    roundEntry: function(value) {
      var val = parseFloat(value);
      var roundN = 3;
      return String(Math.round(val * Math.pow(10, roundN)) / Math.pow(10, roundN));

    },

    formatRows: function() {
      var self = this;
      if (this.Node === null)
        return;
      $.each(this.Node.find("tr"), function() {
        //make sure to not overwrite the selected property
        var thisId = $(this).find("#key").html();
        if (self.selectedNode === null || thisId != self.selectedNode.id) {
          var value = $(this).attr("data-content");
          if (isNumber(value))
            $(this).find("#value").html(self.roundEntry(value));
        }

      });
    }
  });

  var isNumber = function(value) {
    return /^-?\d+.?(\d+)?(e-\d+)?$/.test(value);
  }

  return Properties;
});
