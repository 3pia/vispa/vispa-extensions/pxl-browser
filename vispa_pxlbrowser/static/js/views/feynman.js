require.config({
  shim: {
    "extensions/pxlbrowser/static/js/vendor/sigmajs/sigma": ["jquery"]
  }
});

define(["jquery", "emitter", "./../window",
  "extensions/pxlbrowser/static/js/helpers",
  "extensions/pxlbrowser/static/js/vendor/sigmajs/sigma"
], function($, Emitter, Window, Helpers) {
  var Feynman = Emitter._extend({
    init: function(manager) {
      /*** mandatory***/
      this.instance = manager.instance;
      this.manager = manager;
      this.name = null;
      this.window = null;
      this.Node = null;
      /********/

      this.structure = null;
      this.s = null;
      this.sSettings = {
        //renderer settings
        font: "bold",
        //zooming options
        doubleClickZoomingRatio: 2.0,
        zoomingRatio: 1.3,
        dragTimeout: 50,
        defaultEdgeLabelSize: 14,
        maxNodeSize: 3,
        minEdgeSize: 0.8,
        maxEdgeSize: 4,
        enableEdgeHovering: this.instance.prefs.get("ClickableEdges"),
        edgeHoverColor: '#000'
      };
      this.refreshTimeOut = null;
      this.isSelected = true;
      this.isFlipped = false;
    },
    /***mandatory function***/
    setup: function(name) {
      this.name = name;
      this.window = new Window(this.manager, name, this);
      var self = this;
      this.instance.on("changedState", function(key) {
        switch (key) {
          case "selectedID":
            var ID = self.instance.state.get("selectedID");
            if (ID) {
              self.isSelected = true;
              self.highlightEdge(ID);
            } else {
              self.isSelected = false;
              self.resetGraphColors();
            }
            break;
        }
      });
      return this;
    },
    /***mandatory function***/
    reset: function() {
      if (this.Node) {
        this.Node.empty();
      } else
        this.Node = $("<div />").addClass("windowcontent");
      this.Node.append($("<div />").css({
        height: "100%",
        width: "100%"
      }));
      var controlBar = this.makeControlBar();
      this.Node.append(controlBar);
      this.s = null;
      return this;
    },
    /***mandatory function***/
    update: function() {
      var self = this;
      this.reset();
      var cb = function(err) {
        if (err) {
          console.log("Something went wrong while loading the feynman graph.");
          return;
        } else
          self.update();
      }
      var res = this.manager.instance.cache.getEntry("feynman", cb);
      if (res.available) {
        this.structure = res.content;
        this.structure = {
          nodes: this.setNodes(this.structure.YByX),
          edges: this.setEdges()
        };
        this.window.setContent(this.Node);
        this.doWhenAppend();
      }
    },
    /***mandatory function***/
    doWhenAppend: function(zoomLevel) {
      //make the sigma graph
      if (this.s) {
        this.s.kill();
      }
      this.sSettings["enableEdgeHovering"] = this.instance.prefs.get("ClickableEdges");
      this.s = new sigma({
        graph: this.structure,
        renderer: {
          container: this.Node.children()[0],
          type: 'canvas'
        },
        settings: this.sSettings
      });
      this.saveInitialProperties();
      this.setupFunctionality();
      this.s.cameras[0].ratio = zoomLevel === undefined ? 1.05 : zoomLevel;
      this.s.refresh();
      this.highlightEdge();
      return this;
    },
    /***mandatory function***/
    afterResize: function() {
      if (!this.s)
        return;
      this.doWhenAppend(this.s.cameras[0].ratio); //keep the zoomLevel
    },


    /***********************************************************************/
    /************View specific functions********************/
    /***********************************************************************/

    highlightEdge: function(ID) {
      ID = ID ? ID : this.instance.state.get("selectedID");
      if (!this.s || !this.isSelected) return;
      var edge = this.s.graph.IdToEdge(ID);
      if (!edge) { //if node does not exist, do not highlight anything
        this.resetGraphColors();
        return;
      }

      this.s.graph.nodes().forEach(function(n) {
        if (n.id == edge.source || n.id == edge.target)
          n.color = n.originalColor;
        else
          n.color = "#eee";
      });

      this.s.graph.edges().forEach(function(e) {
        if (e.id == ID)
          e.color = e.originalColor;
        else
          e.color = "#eee";
      });
      this.s.refresh();
    },

    makeControlBar: function() {
      var self = this;
      var node = Helpers.getControlBarNode();
      $.each(node.find("a"), function() {
        var action = $(this).attr("id").split("-").pop();
        $(this).click(function() {
          Helpers.reactToControl(self, action);
        });
      });
      return node;
    },


    performCut: function(p) {
      if (this.instance.Sidebar.isGood(p))
        return "1.0)";
      else
        return "0.10)";
    },

    resetGraphColors: function() {
      this.s.graph.nodes().forEach(function(n) {
        n.color = n.originalColor;
      });

      this.s.graph.edges().forEach(function(e) {
        e.color = e.originalColor;
      });
      this.s.refresh();
    },

    saveInitialProperties: function() {
      //save the initially set colors
      this.s.graph.nodes().forEach(function(n) {
        n.originalColor = n.color;
      });
      this.s.graph.edges().forEach(function(e) {
        e.originalColor = e.color;
      });
    },

    setEdges: function() {
      var edges = [];
      var sources = {};

      for (i in this.structure.edges) {
        var currentEdge = this.structure.edges[i];
        var s = currentEdge.source
        var t = currentEdge.target;
        var s_t = sources[t];
        if (s_t === undefined)
          sources[t] = [];
        s_t = sources[t].indexOf(s) != -1;
        if (sources[s])
          s_t = s_t || sources[s].indexOf(t) != -1;

        var selfLoop = currentEdge.source == currentEdge.target || s_t;
        var colourScheme = this.instance.Sidebar.getScheme();
        edges.push({
          id: currentEdge.id,
          label: currentEdge.name,
          source: s,
          target: t,
          size: 1.,
          color: Helpers.assignColor(colourScheme, currentEdge.name) + this.performCut(currentEdge),
          type: Helpers.assignEdgeType(currentEdge.name, selfLoop)
        });
        sources[t].push(s);


      }
      return edges;
    },

    setNodes: function(YByX) {
      var nodes = [];
      var positions = this.structure.positions;

      //correctly rescale the graph
      var w_d = this.window.getDimensions();
      var alpha = this.isFlipped ? YByX : 1. / YByX;
      alpha *= w_d.w / w_d.h;

      for (var key in positions) {
        var x = this.isFlipped ? positions[key].x : positions[key].y
        x *= alpha;
        var y = this.isFlipped ? positions[key].y : positions[key].x
        var name = positions[key].name;
        var level = positions[key].level;
        nodes.push({
          id: key,
          x: x,
          y: y,
          size: 1,
          color: "rgb(200,200,200)"
        });
      }
      return nodes;
    },

    setupFunctionality: function() {
      var self = this;
      this.s.bind("clickEdge", function(e) {
        var ID = e.data.edge.id;
        self.isSelected = true;
        if (self.instance.state.get("selectedID") == ID)
          self.highlightNeighborhood();
        else
          self.instance.state.set("selectedID", ID);
      });
      this.s.bind("clickStage", function() {
        self.instance.state.set("selectedID", null);
      });
    }
  });



  return Feynman;
});