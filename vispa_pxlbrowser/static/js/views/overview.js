require.config({
  shim: {
    "extensions/pxlbrowser/static/js/vendor/treegrid/js/jquery.treegrid.min": ["jquery"]
  }
});

define(["jquery", "emitter", "./../window", "css!../../css/views/overview", "css!../vendor/treegrid/css/jquery.treegrid.css"], function($, Emitter, Window) {

  var nameOverview = {
    init: function(owner) {
      this.owner = owner;
      this.content = [];
      this.$node = null;
      this.defaults = null;
      this.state = null;
    },

    setContent: function(names) {
      this.content = [];
      for (var name in names) {
        this.pushContent(name, names[name], 1);
      }
    },

    pushContent: function(name, children, depth) {
      this.content.push([name, depth]);
      for (var n in children) {
        this.pushContent(n, children[n], depth + 1);
      }
    },

    setDefaults: function(def) {
      if (this.defaults === null) {
        this.defaults = def;
        this.state = def;
      }
    },

    getBody: function() {
      var self = this;
      var $node = $("<div/>");

      $node.append($("<table/>").addClass("table").addClass("table-bordered"));
      $node.find("table").append($("<thead/>")
        .append($("<tr/>")
          .append($("<th/>").html("Name"))
          .append($("<th/>").html("Show").css("text-align", "center"))
          .append($("<th/>").html("Show if indexed").css("text-align", "center"))
          .append($("<th/>").html("Hide").css("text-align", "center"))
        )
      );

      $node.find("table").append($("<tbody/>"));
      for (var i = 0; i < this.content.length; i++) {
        $node.find("tbody").append(this.makeLine(this.content[i]));
      }

      $node.find("input").each(function(index, element) {
        $(element).click(function() {});
      });
      this.$node = $node;
      return $node;
    },

    getFooter: function() {
      var self = this;

      var default_btn = $("<button/>").attr({
        type: "button",
      }).addClass("btn btn-default").click(function() {
        self.applyDefaults();
      }).append($("<i/>").addClass("glyphicon glyphicon-erase")).append("Apply defaults");

      var all_btn = $("<button/>").attr({
        type: "button",
      }).addClass("btn btn-default").click(function() {
        self.selectAll("all");
      }).append($("<i/>").addClass("glyphicon glyphicon-star")).append("All");

      var indexed_btn = $("<button/>").attr({
        type: "button",
      }).addClass("btn btn-default").click(function() {
        self.selectAll("indexed");
      }).append($("<i/>").addClass("glyphicon glyphicon-tag")).append("Indexed");

      var none_btn = $("<button/>").attr({
        type: "button",
      }).addClass("btn btn-default").click(function() {
        self.selectAll("none");
      }).append($("<i/>").addClass("glyphicon glyphicon-star-empty")).append("None");

      return $("<div/>").addClass("input-group")
        .append(default_btn)
        .append(all_btn)
        .append(indexed_btn)
        .append(none_btn);
    },

    makeLine: function(element) {
      var $node = $("<tr/>");
      $node.append($("<td/>").html(element[0]).css("font-weight", element[1] == 1 ? "bold" : "normal"));

      //the Show radio box
      var attribute = {
        type: "radio",
        name: element[0] + "_" + element[1],
        value: element[0] + "_/_show",
        checked: "checked"
      };

      $node.append(
        $("<td/>")
        .append(
          $("<div/>")
          .addClass("radio")
          .css("text-align", "center")
          .append($("<label/>").append(
            $("<input/>")
            .attr(attribute)
          ))
        )
      );

      //the Show If Indexed radio box
      attribute = {
        type: "radio",
        name: element[0] + "_" + element[1],
        value: element[0] + "_/_showIfIndexed"
      };
      if (this.state["indexed"].indexOf(element[0]) != -1)
        attribute["checked"] = "checked";

      $node.append(
        $("<td/>")
        .append(
          $("<div/>")
          .addClass("radio")
          .css("text-align", "center")
          .append($("<label/>").append(
            $("<input/>")
            .attr(attribute)
          ))
        )
      );

      //the Hide radio box
      attribute = {
        type: "radio",
        name: element[0] + "_" + element[1],
        value: element[0] + "_/_hide"
      }
      if (this.state["hide"].indexOf(element[0]) != -1)
        attribute["checked"] = "checked";
      $node.append(
        $("<td/>")
        .append(
          $("<div/>")
          .addClass("radio")
          .css("text-align", "center")
          .append($("<label/>").append(
            $("<input/>")
            .attr(attribute)
          ))
        )
      );

      return $node;
    },

    applyDefaults: function() {
      var self = this;

      this.$node.find("input").each(function(index, inp) {
        var prop = $(inp).attr("value").split("_/_");

        var check = false;
        if (prop[1] == "show") {
          check = (self.defaults["hide"].indexOf(prop[0]) == -1) && (self.defaults["indexed"].indexOf(prop[0]) == -1);
        } else if (prop[1] == "showIfIndexed") {
          check = (self.defaults["indexed"].indexOf(prop[0]) != -1);
        } else if (prop[1] == "hide") {
          check = (self.defaults["hide"].indexOf(prop[0]) != -1);
        }

        $(inp).prop("checked", check);
      });
    },

    selectAll: function(mode) {

      this.$node.find("input").each(function(index, inp) {
        var prop = $(inp).attr("value").split("_/_");

        if (prop[1] == "show") {
          $(inp).prop("checked", mode == "all" ? true : false);
        } else if (prop[1] == "showIfIndexed") {
          $(inp).prop("checked", mode == "indexed" ? true : false);
        } else if (prop[1] == "hide") {
          $(inp).prop("checked", mode == "none" ? true : false);
        }
      });
    },

    parseFilter: function() {
      var indexed = [];
      var hide = [];

      this.$node.find("input").each(function(index, inp) {
        var prop = $(inp).attr("value").split("_/_");
        var checked = $(inp).is(':checked');
        if (checked) {
          if (prop[1] == "showIfIndexed") indexed.push(prop[0]);
          else if (prop[1] == "hide") hide.push(prop[0]);
        }
      });

      this.state = {
        indexed: indexed,
        hide: hide
      }
      return {
        indexed: this.state["indexed"].join(";"),
        hide: this.state["hide"].join(";")
      }
    }
  }



  var Overview = Emitter._extend({
    init: function(manager) {
      /*** mandatory***/
      this.instance = manager.instance;
      this.manager = manager;
      this.name = null;
      this.window = null;
      this.Node = null;
      /********/

      this.PlacementCounter = 0;
      this.treeNodes = [];
      this.positionCounter = [];

      this.selectedID = this.instance.state.get("selectedID");
      this.oldSelected = null;
    },
    /***mandatory function***/
    setup: function(name) {
      this.name = name;
      nameOverview.init(this);
      this.window = new Window(this.manager, name, this);
      require(["extensions/pxlbrowser/static/js/vendor/treegrid/js/jquery.treegrid.min"]);
      var self = this;
      this.instance.on("changedState", function(key) {
        switch (key) {
          case "selectedID":
            var ID = self.instance.state.get("selectedID");
            if (ID) {
              self.selectedID = ID;
              self.toggleColors();
            }
            break;
        }
      });
      return this;
    },
    /***mandatory function***/
    reset: function() {
      this.AlignmentCounter = 0;
      this.positionCounter = [];
      this.treeNodes = [];

      if (this.Node)
        this.Node.empty();
      else
        this.Node = $("<div />").addClass("windowcontent");

      return this;
    },
    /***mandatory function***/
    update: function() {
      var self = this;

      this.reset();

      var cb = function(err) {
        if (err) {
          console.log("Something went wrong while loading the overview.");
          return;
        } else
          setTimeout(function() {
            self.update();
          }, 500); //allow the answer tp be transmitted via websockets
      }

      //make the header
      var $header = this.makeHeader();
      this.Node.append($header);

      var res = this.manager.instance.cache.getEntry("overview", cb);
      if (res.available) {
        this.PlacementCounter = 1;

        var treeNodes = this.createTreeNodes(res.content, this.PlacementCounter, 0);
        var tree = $("<table />").attr("id", "tree")
          .append(treeNodes)
          .treegrid();
        this.Node.append(
          $("<div/>").css({
            height: "calc(100% - 25px)",
            width: "100%",
            overflow: "auto"
          }).append(tree));
        this.selectSelected(); //scroll to the selected item  

        this.window.setContent(this.Node);
        this.doWhenAppend();
      }

      return this;
    },
    /***mandatory function***/
    doWhenAppend: function() {
      return this;
    },
    /***mandatory function***/
    afterResize: function() {},


    /***********************************************************************/
    /************View specific functions********************/
    /***********************************************************************/

    makeHeader: function() {
      var self = this;
      var text = "No Filter";
      var activeFilter = (this.manager.instance.prefs.get("ShowIndexedObjects").split(";") 
        + this.manager.instance.prefs.get("HideObjects").split(";")) != "";
      var css = {};
      if (activeFilter){
        css["color"] = "red";
        text = "Active Filters!";
      }
      return $("<div/>").addClass("header").css(css).html(text).click(function() {
        self.showNames();
      });
    },

    showNames: function() {
      var self = this;

      var cb = function(err) {
        if (err) {
          console.log("Something went wrong while loading the overview names.");
          return;
        } else
          setTimeout(function() {
            self.showNames();
          }, 500); //allow the answer tp be transmitted via websockets
      }

      nameOverview.setDefaults({
        indexed: self.manager.instance.prefs.get("ShowIndexedObjects").split(";"),
        hide: self.manager.instance.prefs.get("HideObjects").split(";")
      });

      var res = this.manager.instance.cache.getEntry("overviewNames", cb);
      if (res.available) {
        nameOverview.setContent(res.content);
        this.instance.dialog({
          header: "<i class='glyphicon glyphicon-info-sign'></i> Overview Filter",
          body: nameOverview.getBody(),
          footer: nameOverview.getFooter(),
          beforeClose: function(cb) {
            var filters = nameOverview.parseFilter();
            self.manager.instance.prefs.set("ShowIndexedObjects", filters["indexed"]);
            self.manager.instance.prefs.set("HideObjects", filters["hide"]);
            self.manager.instance.pushPreferences();
            self.manager.instance.POST("setFilter", {
              indexed_objects: filters["indexed"],
              forbidden_strings: filters["hide"]
            }, function() {
              self.manager.instance.cache.clearCache("overview");
              self.update();
              if ($.isFunction(cb)) cb();
            });

          }
        });
      }
    },


    createTreeNodes: function(obj, id, mother) {
      var self = this;
      var nodes = [];
      var label = obj.name || obj.type;

      var tr = $("<tr />").addClass("treegrid-" + String(id));
      if (mother > 0)
        tr.addClass("treegrid-parent-" + String(mother));
      var cssClass = obj.indexes.length > 0 ? "treenode_indexed" : "treenode";
      var node = $("<a />")
        .attr("id", obj.id)
        .addClass(cssClass)
        .click(function() {
          self.instance.state.set("selectedID", obj.id);
        }).on({
          mouseover: function() {
            $("body").css("cursor", "pointer");
          },
          mouseleave: function() {
            $("body").css("cursor", "default");
          }
        });
      //set the indexes, if available, as titles:
      if (obj.indexes.length > 0) {
        label += ": ";
        var title = "Indexes: ";
        for (var i = 0; i < obj.indexes.length; i++) {
          title += obj.indexes[i];
          label += obj.indexes[i];
          if (i != obj.indexes.length - 1) {
            label += ", "
            title += ", "
          }
        }
        node.attr("title", title);
      }
      node.html(label);

      //count the occurence of 'label'
      this.positionCounter[label] = !this.positionCounter[label] ? 1 :
        this.positionCounter[label] + 1;

      $("<td />").append(node).appendTo(tr);

      this.treeNodes[obj.id] = {
        node: node,
        label: label,
        relPos: this.positionCounter[label]
      };

      nodes.push(tr);

      //loop through all remaining children recursively
      $.each(obj.children.elements, function(i, child) {
        if (Object.keys(child).length === 0) return;
        self.PlacementCounter = self.PlacementCounter + 1;
        $.each(self.createTreeNodes(child, self.PlacementCounter, id), function(j, childNode) {
          nodes.push(childNode);
        });
      });

      return nodes;
    },

    //changes the color of the selected object and stores the selected element
    toggleColors: function(ID) {
      ID = !ID ? this.instance.state.get("selectedID") : ID;

      if (this.oldSelected) {
        $(this.oldSelected.node).removeClass("treenode_selected");
      }

      if (this.treeNodes[ID] !== undefined) {
        $(this.treeNodes[ID].node).addClass("treenode_selected");
        this.oldSelected = this.treeNodes[ID];
      }

      return this;
    },

    selectSelected: function() { //trigger a click?
      var ID = this.instance.state.get("selectedID");
      if (!ID)
        return this;
      this.oldSelected = this.oldSelected ? this.oldSelected : this.treeNodes[ID]
      if (!this.oldSelected)
        return this;

      var label = this.oldSelected.label;
      var relPos = this.oldSelected.relPos;

      if (!this.positionCounter[label] || this.positionCounter[label] < relPos)
        return this;

      for (var ID in this.treeNodes) {
        var _label = this.treeNodes[ID].label;
        var _relPos = this.treeNodes[ID].relPos;
        if (_label == label && _relPos == relPos)
          this.selectElement(ID);
      }
      return this;
    },

    selectElement: function(ID) {
      if (ID != this.instance.state.get("selectedID"))
        this.instance.state.set("selectedID", ID);
      else
        this.toggleColors();
      var el = this.treeNodes[ID].node;
      this.expandBranch(el.parent().parent());
      this.scrollToElement(el, 100); //100ms delay to let grid expand first
      return this;
    },

    scrollToElement: function(el, delay) {
      
      this.scrollTimeOut = setTimeout(function() {
        $(el).get(0).scrollIntoView();
      }, delay !== undefined ? delay : 1000);
      
      return this;
    },

    expandBranch: function(tr) {
      if (tr === null)
        return this;
      tr.treegrid("expand");
      this.expandBranch(tr.treegrid("getParentNode"));
    },

    giveSelectedId: function() {
      var ID = this.Node.find(".treenode_selected")
        .attr("id");
      return ID;
    },

    selectedRelPos: function() {
      if (this.isSelected)
        return this.treeNodes[this.selectedID].relPos;
    },
    selectedLabel: function() {
      if (this.isSelected)
        return this.treeNodes[this.selectedID].label;
    },
    isSelected: function() {
      return this.treeNodes[this.selectedID] ? true : false;
    }
  });
  return Overview;
})
