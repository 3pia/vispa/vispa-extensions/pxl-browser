define(["jquery", "emitter", "./../window"], function($, Emitter, Window) {

  var View3D = Emitter._extend({
    init: function(manager) {
      /*** mandatory***/
      this.instance = manager.instance;
      this.manager = manager;
      this.name = null;
      this.window = null;
      this.Node = null;
      /********/

      this.selectedNode = null;

      //need unique IDs for collapsing single tables
      this.timestamp = String((new Date()).getTime());
      this.currentlyVisible = ["Particle" + this.timestamp]; //table headers that are by default shown
    },
    /***mandatory function***/
    setup: function(name) {
      this.name = name;
      this.window = new Window(this.manager, name, this);
      var self = this;
      this.instance.on("changedState", function(key) {
        switch (key) {
          case "selectedID":
            var ID = self.instance.state.get("selectedID");
            if (ID)
              self.update();
            break;
        }
      });
      this.generateContent();
      return this;
    },
    /***mandatory function***/
    reset: function() {
      if (this.Node)
        this.Node.empty();
      else
        this.Node = $("<div />").addClass("windowcontent");
      return this;
    },
    /***mandatory function***/
    update: function() {
      var self = this;

      // this.reset();

      var objID = this.instance.state.get("selectedID");
      if (!objID) {
        this.window.setContent(this.Node);
        this.doWhenAppend();
        return;
      }

      var cb = function(err) {
        if (err){
          console.log("Something went wrong while loading the 3D View.");
          return;
        }
        else
          self.update();
      }
      var res = this.manager.instance.cache.getEntry(objID, cb);
      if (res.available) {
        // this.generateCutSlider(res.content);
        this.window.setContent(this.Node);
        this.doWhenAppend();
      }
    },
    /***mandatory function***/
    doWhenAppend: function() {
      return this;
    },
    /***mandatory function***/
    afterResize: function() {},


    /***********************************************************************/
    /************View specific functions********************/
    /***********************************************************************/


    generateContent: function(data) {
      var self = this;

      this.reset();

      this.Node.addClass("container-3dview");


    },









  });

  return View3D;
});