require.config({
  shim: {
    "extensions/pxlbrowser/static/js/vendor/sigmajs/sigma": ["jquery"]
  }
});

define(["jquery", "emitter", "./../window", "extensions/pxlbrowser/static/js/helpers",
  "extensions/pxlbrowser/static/js/vendor/sigmajs/sigma"
], function($, Emitter, Window, Helpers) {
  var Relations = Emitter._extend({
    init: function(manager) {
      /*** mandatory***/
      this.instance = manager.instance;
      this.manager = manager;
      this.name = null;
      this.window = null;
      this.Node = null;
      /********/

      this.structure = null;
      this.s = null;
      this.sSettings = {
        //renderer settings
        font: "bold",
        labelThreshold: 6,
        //zooming options
        doubleClickZoomingRatio: 2.0,
        zoomingRatio: 1.3,
        dragTimeout: 50
      };
      this.refreshTimeOut = null;
      this.isSelected = true;
      this.isFlipped = false;

    },
    /***mandatory function***/
    setup: function(name) {
      this.name = name;
      this.window = new Window(this.manager, name, this);
      var self = this;
      this.instance.on("changedState", function(key) {
        switch (key) {
          case "selectedID":
            var ID = self.instance.state.get("selectedID");
            if (ID) {
              self.isSelected = true;
              self.highlightNeighborhood(ID);
            } else {
              self.isSelected = false;
              self.resetGraphColors();
            }
            break;
        }
      });
      return this;
    },
    /***mandatory function***/
    reset: function() {
      if (this.Node) {
        this.Node.empty();
      } else
        this.Node = $("<div />").addClass("windowcontent");
      this.Node.append($("<div />").css({
        height: "100%",
        width: "100%"
      }));
      var controlBar = this.makeControlBar();
      this.Node.append(controlBar);
      this.s = null;

      return this;
    },
    /***mandatory function***/
    update: function() {
      var self = this;

      this.reset();
      var cb = function(err) {
        if (err) {
          console.log("Something went wrong while loading the relation graph.");
          return;
        } else
          self.update();
      }
      var res = this.manager.instance.cache.getEntry("tree", cb);
      if (res.available) {
        this.structure = res.content;
        this.structure = {
          nodes: this.setNodes(this.structure.YByX),
          edges: this.setEdges()
        };
        this.window.setContent(this.Node);
        this.doWhenAppend();
      }
    },
    /***mandatory function***/
    doWhenAppend: function(zoomLevel) {
      if (this.s) {
        this.s.kill();
      }
      //make the sigma graph
      this.s = new sigma({
        graph: this.structure,
        container: this.Node.children()[0],
        settings: this.sSettings
      });
      this.saveInitialProperties();
      this.setupFunctionality();
      this.s.cameras[0].ratio = zoomLevel === undefined ? 1.05 : zoomLevel;
      this.s.refresh();
      this.highlightNeighborhood();
      return this;
    },
    /***mandatory function***/
    afterResize: function() {
      if (!this.s)
        return;
      this.doWhenAppend(this.s.cameras[0].ratio); //keep the zoomLevel
    },


    /***********************************************************************/
    /************View specific functions********************/
    /***********************************************************************/

    addEdge: function(node, edges, edgeIDs) {
      var daughters = node.children;
      if (daughters.length === 0)
        return edges;
      var source = node.key;

      for (var i = 0; i < daughters.length; i++) {
        var target = daughters[i].key;
        var edgeID = source + "_" + target;
        if (edgeIDs.indexOf(edgeID) != -1) //no double assignments
          continue;
        edgeIDs.push(edgeID);
        edges.push({
          id: edgeID,
          source: source,
          target: target,
          size: 2,
          color: "rgb(128,128,128)"
        });
        this.addEdge(daughters[i], edges, edgeIDs);
      }
      return edges;
    },

    highlightNeighborhood: function(ID) {
      ID = ID ? ID : this.instance.state.get("selectedID");
      if (!this.s || !this.isSelected) return;
      var node = this.s.graph.IdToNode(ID);
      if (!node) { //if node does not exist, do not highlight anything
        this.resetGraphColors();
        return;
      }
      var toKeep = this.s.graph.neighbors(ID);
      toKeep[ID] = node;

      this.s.graph.nodes().forEach(function(n) {
        if (toKeep[n.id])
          n.color = n.originalColor;
        else
          n.color = "#eee";
      });

      this.s.graph.edges().forEach(function(e) {
        if ((e.source == ID && toKeep[e.target]) || (e.target == ID && toKeep[e.source]))
          e.color = e.originalColor;
        else
          e.color = "#eee";
      });
      this.s.refresh();
    },

    makeControlBar: function() {
      var self = this;
      var node = Helpers.getControlBarNode();
      $.each(node.find("a"), function() {
        var action = $(this).attr("id").split("-").pop();
        $(this).click(function() {
          Helpers.reactToControl(self, action);
        });
      });
      return node;
    },

    performCut: function(p) {
      if (this.instance.Sidebar.isGood(p))
        return 3;
      else
        return 1;
    },

    resetGraphColors: function() {
      this.s.graph.nodes().forEach(function(n) {
        n.color = n.originalColor;
      });

      this.s.graph.edges().forEach(function(e) {
        e.color = e.originalColor;
      });
      this.s.refresh();
    },

    saveInitialProperties: function() {
      //save the initially set colors
      this.s.graph.nodes().forEach(function(n) {
        n.originalColor = n.color;
      });
      this.s.graph.edges().forEach(function(e) {
        e.originalColor = e.color;
      });
    },

    setEdges: function() {
      var edges = [];
      var edgeIDs = [];
      var tree = this.structure.tree;
      for (var i = 0; i < tree.length; i++) {
        edges = this.addEdge(tree[i], edges, edgeIDs);
      }
      return edges;
    },

    setNodes: function(YByX) {
      var nodes = [];
      var positions = this.structure.positions;

      //correctly rescale the graph
      var w_d = this.window.getDimensions();
      var alpha = this.isFlipped ? YByX : 1. / YByX;
      alpha *= w_d.w / w_d.h;

      //positioning of the dummy
      var xMin = 100000,
        yMean = 0,
        N = 0;

      var colourScheme = this.instance.Sidebar.getScheme();
      for (var key in positions) {
        var x = this.isFlipped ? positions[key].x : positions[key].y
        x *= alpha;
        var y = this.isFlipped ? positions[key].y : positions[key].x
        var name = positions[key].name;
        nodes.push({
          id: key,
          label: name,
          x: x,
          y: y,
          size: this.performCut(positions[key]),
          color: Helpers.assignColor(colourScheme, name) + "1.0)"
        });
        N += 1;
        xMin = Math.min(x, xMin);
        yMean += y;
      }

      //a dummy node to keep the node sizes for the cuts (numbers are only relative sizes!)
      nodes.push({
        id: "dummy",
        label: "",
        x: xMin,
        y: yMean/N,
        size: 3,
        color: "rgba(255, 255, 255, 1.0)"
      })
      return nodes;
    },

    setupFunctionality: function() {
      var self = this;

      //also from websites example
      this.s.bind("clickNode", function(e) {
        var ID = e.data.node.id;
        if (ID == "dummy") return;
        self.isSelected = true;
        if (self.instance.state.get("selectedID") == ID)
          self.highlightNeighborhood();
        else
          self.instance.state.set("selectedID", ID);
      });

      this.s.bind("clickStage", function() {
        self.instance.state.set("selectedID", null);
      });
    }
  });
  return Relations;
});