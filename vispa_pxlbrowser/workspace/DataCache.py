from helpers import Tree
from helpers import Feynman
import logging
import copy

logger = logging.getLogger(__name__)

def getNames(obj):
    children = {}
    for o in obj["children"]["elements"]:
        children[o["name"]] = getNames(o)

    name = obj["name"] if obj["name"] else "Event"

    return children

class DataCache:
    def __init__(self):
        self._overview={}   #contains all objects in the event
        self._overview_names = {}
        self._idDict={}
        self._tree = None
        self._feynman = None
        
        self._cache = {}

        self.indexed_objects = ""
        self.forbidden_strings = ""

    ''' Functions called by the controller for steering the cache  '''
    def loadEventObjects(self, event):
        logger.info("setting the event object")
        self._addChildObject(event) #recursively adding objects to the id list
        self._overview=event.createDataStructure()
        self._overview_names=getNames(self._overview)
        self._tree=(Tree.Tree(self._idDict)).get_tree()
        self._feynman=(Feynman.Feynman(self._idDict)).get_feynman()
              
    def clear_event(self):
        logger.info("clearing the cache")
        for obj in self._idDict.values():
            del obj
        self._idDict={}
        self._tree=None
        self._feynman=None

    def clear_cache(self):
        logger.info("clearing all")
        self.clear_event()
        for obj in self._cache:
            del obj
        self._cache = None
    
    def check_cache(self, ID):
        ID = str(ID)
        logger.info("check existence in cache of key "
            +ID+": "+str(self._cache.has_key(ID)))
        if self._cache.has_key(str(ID)):
            return True
        else:
            return False

    def memorize_current_event(self, ID):       
        logger.info("memorizing current cache with key "+str(ID))
        self._cache[str(ID)] = {
            'overview': self._overview,
            'tree': self._tree,
            'feynman': self._feynman,
            'idDict': self._idDict
        }

    def load_from_cache(self, ID):
        ID = str(ID)
        logger.info("loading key " + ID + " from cache")
        self.clear_event()
        self._overview = self._cache[ID]['overview']
        self._idDict = self._cache[ID]['idDict']
        self._tree = self._cache[ID]['tree']
        self._feynman = self._cache[ID]['feynman']

    '''Internal functions'''
    def _addChildObject(self, object):
        self._idDict[object.getId()]=object
        for child in object.getChildren():
            self._addChildObject(child)

    def _filter_overview(self, obj):
        if (self._filter(obj)):
            obj.clear()
        else:
            for child in obj["children"]["elements"]:
                self._filter_overview(child)

    def _filter(self, obj):
        filtered_name = False
        if self.forbidden_strings != "":
            for name in self.forbidden_strings.split(";"):
                if name == obj["name"]:
                    filtered_name = filtered_name or True
                    break

        filtered_index = False
        if self.indexed_objects != "":
            for name in self.indexed_objects.split(";"):
                if name == obj["name"] and len(obj["indexes"]) == 0:
                    filtered_index = True
                    break

        return filtered_name or filtered_index

    ''' Explicit getter functions'''
    def getOverview(self):
        logger.info("getting the cache")
        ovw = copy.deepcopy(self._overview)
        
        for child in ovw["children"]["elements"]:
            self._filter_overview(child)
        
        return {
            "overview": ovw,
            "names": self._overview_names
        }

    def getTree(self):
        logger.info("getting the tree")
        return self._tree

    def getFeynman(self):
        logger.info("getting the feynman graph")
        return self._feynman

    def getObjectProperties(self, oid):
        logger.info("getting object properties")
        try:
            return self._idDict[oid].getPropertyList()
        except:
            return []

    def setDisplayLimits(self, indexed_objects, forbidden_strings):
        logger.info("Setting the overview display limits")
        self.indexed_objects = indexed_objects
        self.forbidden_strings = forbidden_strings