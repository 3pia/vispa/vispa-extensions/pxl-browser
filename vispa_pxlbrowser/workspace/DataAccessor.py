'''Original code from VISPA 0.5, no modifications have been applied'''
#import cherrypy
from pxl import core, hep, astro
import random
from math import isinf
import logging
logger = logging.getLogger(__name__)

try:
    from pxl.healpix import *
    Healpix_initialize()
    logging.getLogger("extension.pxlbrowser.dataaccessor").info("pxl.healpix initialized")
except ImportError: 
    pass


class DataAccessor(object):
    @staticmethod      
    def canReadObject(object):
        return False

    @staticmethod
    def getProperties(object):
        pass

    @staticmethod
    def getId(object):
        pass

    @staticmethod
    def getChildren(object):
        pass

    @staticmethod
    def getType():
        return object

    @staticmethod
    def getName(object):
        return None

    @staticmethod
    def getIndexes(object):
        idxs = []
        try:
            for key, value in object.owner().getIndexEntry().items():                
                if value==object:
                    idxs.append(key)
            return idxs
        except:
            return []


class PxlEventDataAccessor(DataAccessor):
    @staticmethod
    def canReadObject(object):
        return isinstance(object, core.Event)

    @staticmethod
    def getProperties(object):
        event = core.toEvent(object)
        infoDict= []
        return infoDict

    @staticmethod
    def getId(object):
        return core.toEvent(object).getId().toString()

    @staticmethod
    def getChildren(object):
        event=core.toEvent(object)
        infoDict=[]
        for child in event.getObjects():
            infoDict.append(DataObject(child))
        return infoDict

    @staticmethod
    def getType():
        return core.Event


class PxlEventViewDataAccessor(DataAccessor):
    @staticmethod
    def canReadObject(object):
        return isinstance(object, hep.EventView)

    @staticmethod
    def getProperties(object):
        eventView=hep.toEventView(object)
        infoDict=[]
        return infoDict

    @staticmethod
    def getId(object):
        return hep.toEventView(object).getId().toString()

    @staticmethod
    def getName(object):
        return hep.toEventView(object).getName()

    @staticmethod
    def getChildren(object):
        eventView=hep.toEventView(object)
        infoDict=[]
        for child in eventView.getObjects():
            infoDict.append(DataObject(child))
        return infoDict

    @staticmethod
    def getType():
        return hep.EventView


class PxlParticleAccessor(DataAccessor):
    @staticmethod
    def canReadObject(object):
        return isinstance(object, hep.Particle)

    @staticmethod
    def getProperties(object):
        particle=hep.toParticle(object)
        eta = particle.getEta()
        sign = "+" if eta > 0 else "-"
        eta = sign + 'inf' if isinf(eta) else eta
        infoDict = [
                    {"E": particle.getE(),
                    "doc": particle.getE.__doc__},
                    {"Et": particle.getEt(),
                    "doc": particle.getEt.__doc__},
                    {"Mass": particle.getMass(),
                    "doc": particle.getMass.__doc__},
                    {"Eta": eta,
                    "doc": particle.getEta.__doc__},
                    {"Phi": particle.getPhi(),
                    "doc": particle.getPhi.__doc__},
                    {"P": particle.getP(),
                    "doc": particle.getP.__doc__},
                    {"Pt": particle.getPt(),
                    "doc": particle.getPt.__doc__},
                    {"Px": particle.getPx(),
                    "doc": particle.getPx.__doc__},
                    {"Py": particle.getPy(),
                    "doc": particle.getPy.__doc__},
                    {"Pz": particle.getPz(),
                    "doc": particle.getPz.__doc__},
                    {"Charge": particle.getCharge(),
                    "doc": particle.getCharge.__doc__},
                    {"PdgNumber": particle.getPdgNumber(),
                    "doc": particle.getPdgNumber.__doc__},]
        return infoDict

    @staticmethod
    def getId(object):
        return hep.toParticle(object).getId().toString()

    @staticmethod
    def getName(object):
        return hep.toParticle(object).getName()

    @staticmethod
    def getChildren(object):
        return []

    @staticmethod
    def getType():
        return hep.Particle

class PxlRelativeAccessor(DataAccessor):
    @staticmethod
    def canReadObject(object):
        return isinstance(object, hep.Particle)
    
    @staticmethod
    def getIdList(objectlist):
        list=[]
        for obj in objectlist:
            list.append(obj.id().toString())
        return list
    
    @staticmethod
    def getNameList(objectlist):
        #check here if object has a getName method - not implemented in pxl::Relative
        list=[]
        for obj in objectlist:
            name="None"
            try:
                name = obj.getName()
                name += "?ID=" + obj.id().toString() 
            except:
                pass
            list.append(name)
            
        return list

    @staticmethod
    def getProperties(object):
        relative=core.toRelative(object)
        infoDict = [
                    {"Daughter ids": PxlRelativeAccessor.getIdList(relative.getDaughterRelations().getContainer()),
                    "doc": relative.getDaughterRelations.__doc__ },
                    {"Daughter names": PxlRelativeAccessor.getNameList(relative.getDaughterRelations().getContainer()),
                    "doc": relative.getDaughterRelations.__doc__},
                    {"Mother ids": PxlRelativeAccessor.getIdList(relative.getMotherRelations().getContainer()),
                    "doc": relative.getMotherRelations.__doc__},
                    {"Mother names": PxlRelativeAccessor.getNameList(relative.getMotherRelations().getContainer()),
                    "doc": relative.getMotherRelations.__doc__}#,
                    #{"SoftRelations": []}  //to be filled
                    ]
        return infoDict

    @staticmethod
    def getId(object):
        return object.getId().toString()

    @staticmethod
    def getName(object):
        return object.getName()

    @staticmethod
    def getChildren(object):
        return []

    @staticmethod
    def getType():
        return core.Relative

class UserRecordHelperAccessor(DataAccessor):
    @staticmethod
    def canReadObject(object):
        return isinstance(object,core.UserRecordHelper)

    @staticmethod
    def getProperties(object):
        list = []
        for ur in object.getUserRecords().getContainer().keys():
            list.append({str(ur):str(object.getUserRecord(ur)),
                        "doc": object.getUserRecord.__doc__ +" (arg: "+ str(ur) + ")"})
        return list
    
    @staticmethod
    def getId(object):
        return object.getId().toString()

    @staticmethod
    def getChildren(object):
        return []
    
    @staticmethod
    def getType():
        return core.UserRecordHelper

    @staticmethod
    def getName(object):
        return None

class PxlBasicContainerAccessor(DataAccessor):
    @staticmethod
    def canReadObject(object):
        return isinstance(object, core.BasicContainer)

    @staticmethod
    def getProperties(object):
        container=core.toBasicContainer(object)
        return []

    @staticmethod
    def getId(object):
        return core.toBasicContainer(object).getId().toString()

    @staticmethod
    def getChildren(object):
        container=core.toBasicContainer(object)
        infoDict=[]
        for child in container.getObjects():
            infoDict.append(DataObject(child))
        return infoDict

    @staticmethod
    def getType():
        return core.BasicContainer
    

class PxlAstroBasicObjectAccessor(DataAccessor):
    @staticmethod
    def canReadObject(object):
        return isinstance(object, PxlAstroBasicObjectAccessor.getType())

    @staticmethod
    def getProperties(object):
        obj=astro.toAstroBasicObject(object)
        return [
            {"Time":str(obj.getTime()),
            "doc": obj.getTime.__doc__},
            {"AngularResolution":str(obj.getAngularResolution()),
            "doc": obj.getAngularResolution.__doc__},
            {"Declination":str(obj.getDeclination()),
            "doc": obj.getDeclination.__doc__},
            {"RightAscension":str(obj.getRightAscension()),
            "doc": obj.getRightAscension.__doc__},
            {"GalacticLatitude":str(obj.getGalacticLatitude()),
            "doc": obj.getGalacticLatitude.__doc__},
            {"GalacticLongitude":str(obj.getGalacticLongitude()),
            "doc": obj.getGalacticLongitude.__doc__},
            {"SuperGalacticLatitude":str(obj.getSuperGalacticLatitude()),
            "doc": obj.getSuperGalacticLatitude.__doc__},
            {"SuperGalacticLongitude":str(obj.getSuperGalacticLongitude()),
            "doc": obj.getSuperGalacticLongitude.__doc__}
            ]

    @staticmethod
    def getId(object):
        return astro.toAstroBasicObject(object).getId().toString()

    @staticmethod
    def getChildren(object):
        return []

    @staticmethod
    def getType():
        return astro.AstroBasicObject
    
class PxlAstroObjectAccessor(DataAccessor):
    @staticmethod
    def canReadObject(object):
        return isinstance(object, PxlAstroObjectAccessor.getType())

    @staticmethod
    def getProperties(object):
        obj=astro.toAstroObject(object)
        return [{"SoftRelations":[]}]

    @staticmethod
    def getId(object):
        return astro.toAstroObject(object).getId().toString()

    @staticmethod
    def getChildren(object):
        return []

    @staticmethod
    def getType():
        return astro.AstroObject

class PxlUHECRAccessor(DataAccessor):
    @staticmethod
    def canReadObject(object):
        return isinstance(object, PxlUHECRAccessor.getType())

    @staticmethod
    def getProperties(object):
        uhecr=astro.toUHECR(object)
        return [
            {"Charge":str(uhecr.getCharge()),
            "doc": uhecr.getCharge.__doc__},
            {"Energy":str(uhecr.getEnergy()),
            "doc": uhecr.getEnergy.__doc__},
            {"EnergyError":str(uhecr.getEnergyError()),
            "doc": uhecr.getEnergyError.__doc__},
            {"Mass":str(uhecr.getMass()),
            "doc": uhecr.getMass.__doc__}
            ]
        
    @staticmethod
    def getId(object):
        return astro.toUHECR(object).getId().toString()

    @staticmethod
    def getChildren(object):
        return []

    @staticmethod
    def getType():
        return astro.UHECR

class PxlRegionOfInterestAccessor(DataAccessor):
    @staticmethod
    def canReadObject(object):
        return isinstance(object, PxlRegionOfInterestAccessor.getType())

    @staticmethod
    def getProperties(object):
        roi=astro.toRegionOfInterest(object)
        return [
            {"Name":str(roi.getName()),
            "doc": roi.getName.__doc__}
            ]
        

    @staticmethod
    def getId(object):
        return astro.toRegionOfInterest(object).getId().toString()

    @staticmethod
    def getChildren(object):
        return []

    @staticmethod
    def getType():
        return astro.RegionOfInterest
    
class PxlUHECRSourceAccessor(DataAccessor):
    @staticmethod
    def canReadObject(object):
        return isinstance(object, PxlUHECRSourceAccessor.getType())

    @staticmethod
    def getProperties(object):
        source=astro.toUHECRSource(object)
        return [
            {"Distance":str(source.getDistance()),
            "doc": source.getDistance.__doc__},
            {"Luminosity":str(source.getLuminosity()),
            "doc": source.getLuminosity.__doc__},
            {"Name":str(source.getName()),
            "doc": source.getName.__doc__}
            ]
        
    @staticmethod
    def getId(object):
        return astro.toUHECRSource(object).getId().toString()

    @staticmethod
    def getChildren(object):
        return []

    @staticmethod
    def getType():
        return astro.UHECRSource


class DataObject:
    def __init__(self, objectRef, cache=None):
        self._accessorDict={}
        for accessor in DataAccessor.__subclasses__():
            self._accessorDict[accessor.getType().__module__+"."+accessor.getType().__name__]=accessor

        self._objectRef=objectRef
        self._id=None
        self._type=None 
        self._name = None
        self._indexes=None
        self._propertyList=[]
        self._propertiesLoaded=False
        self._children=[]
        self._summary={}
        self.cache = cache
        
        for mro in type(self._objectRef).__mro__:
            if self._accessorDict.has_key(mro.__module__+"."+mro.__name__):
                accessor=self._accessorDict[mro.__module__+"."+mro.__name__]
                if self._type==None:
                    self._type=str(accessor.getType().__name__)
                if self._id==None:
                    self._id=accessor.getId(self._objectRef)
                if self._name==None:
                    self._name = accessor.getName(self._objectRef)
                if self._indexes==None:
                    self._indexes = accessor.getIndexes(self._objectRef)
                self._children.extend(accessor.getChildren(self._objectRef))
    
    def createDataStructure(self):
        return {
                "type":self.getType(),
                "id":self.getId(),
                "name":self.getName(),
                "children":self.createChildStructure(), 
                "indexes": self.getIndexes()
                }

    def getPXLRelatives(self):
        for mro in type(self._objectRef).__mro__:
            if (mro.__name__ == 'Relative'):
                accessor = self._accessorDict['pxl.core.Relative']
                if not accessor:
                    return
                relatives = accessor.getProperties(self._objectRef)
                return relatives
        return None

    def createChildStructure(self):
        childList=[]
        childSummary={}
        for child in self._children:
            childList.append(DataObject.createDataStructure(child))
            if not childSummary.has_key(child.getType()):
                childSummary[child.getType()]=1
            else:
                childSummary[child.getType()]+=1
        return {"elements":childList,"summary":childSummary}

    def loadProperties(self):
        logger.info("loading properties")
        if not self._propertiesLoaded:
            for mro in type(self._objectRef).__mro__:
                if self._accessorDict.has_key(mro.__module__+"."+mro.__name__):
                    accessor=self._accessorDict[mro.__module__+"."+mro.__name__]
                    self._propertyList.append({str(accessor.getType().__name__):accessor.getProperties(self._objectRef)})
                    self._propertiesLoaded=True
        if not self._propertiesLoaded:
            logging.getLogger("extension.pxlbrowser.dataaccessor").warning("no apropriate data accessor found")
            self._propertyList=[{"objDump":str(self._objectRef)}]
            self._propertiesLoaded=True
            
    def getType(self):
        return self._type

    def getChildren(self):
        return self._children

    def getName(self):
        return self._name

    def getId(self):
        return self._id

    def getIndexes(self):
        return self._indexes

    def getPropertyList(self):
        if not self._propertiesLoaded:
            self.loadProperties()
        return {
            self.getName(): self._propertyList
            }

    def getCutVars(self):
        try:
            pt = self._objectRef.getPt()
            E = self._objectRef.getE()
            Eta = self._objectRef.getEta()
            if (str(Eta) == "inf"):
                Eta = "+inf"
            elif (str(Eta) == "-inf"):
                Eta == "-inf"
        except:
            pt, E, Eta = 0, 0, 0
        return (pt, E, Eta)
