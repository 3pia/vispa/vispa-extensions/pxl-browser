import math
import time
#the algorithm for positioning
from Buchheim import *
import logging
logger = logging.getLogger(__name__)

class Vertex:
    def __init__(self, ID, level):
        self._ID = ID
        self._level = level  #hierarchy level
        self._ins = []
        self._outs = []
        self._children = []
    def _add_child(self, child):
        exists = False
        for i in range(0, len(self._children)):
            if self._children[i]._get_ID() == child._get_ID():
                exists = True
        if not exists:
            self._children.append(child)
    def _add_in(self, ID):
        if not ID in self._ins:
            self._ins.append(ID)
    def _add_out(self, ID):
        if not ID in self._outs:
            self._outs.append(ID)
    def _has_in(self, ID):
        return ID in self._ins
    def _has_out(self, ID):
        return ID in self._outs
    def _get_ID(self):
        return self._ID
    def _get_level(self):
        return self._level
    def _get_children(self):
        return self._children
    def _set_level(self, level):
        self._level = min(level, self._level)

#this class should calculate and contain all relevant data for the tree view
class Feynman:
    def __init__(self, idDict):
        self._positions = {}
        self._edges = {}
        self._object_list = idDict
        self._max_x = 0
        self._max_y = 0
        self._vertices = {}
        self._vertex_count = 0
        start = time.time()
        self._make_structure(self._find_roots(), 1)
        end = time.time()
        logger.info("It took "+str(end-start)+" seconds for positioning")    

    def _find_roots(self):
        if not self._object_list:
            return
        root_objects = []
        for key in self._object_list:
            relatives = self._object_list[key].getPXLRelatives()
            if not relatives:
                continue
            daughters = relatives[0]['Daughter ids']
            mothers = relatives[2]['Mother ids']
            isRoot = len(daughters)>0 and len(mothers)==0
            if (isRoot):
                root_objects.append(key)
        return root_objects

    def _make_structure(self, roots, level):
        root_IDs = []
        for i in range(0, len(roots)):
            key = roots[i]
            obj = self._object_list[key]
            name = obj.getName()
            relatives = obj.getPXLRelatives()
            if not relatives:
                continue
            daughters = relatives[0]['Daughter ids']

            self._vertex_count += 1
            start_vertex = Vertex(str(self._vertex_count), level)        

            end_vertex = None
            for k in self._vertices:
                if self._vertices[k]._has_in(key):
                    end_vertex=self._vertices[k]
            if not end_vertex:
                self._vertex_count += 1
                end_vertex = Vertex(str(self._vertex_count), level+1)
            if end_vertex._get_level > start_vertex._get_level():
                start_vertex._add_child(end_vertex)
            elif end_vertex._get_level < start_vertex._get_level():
                end_vertex._add_child(start_vertex)

            root_IDs.append(start_vertex._get_ID())
            self._vertices[start_vertex._get_ID()] = start_vertex
            self._vertices[end_vertex._get_ID()] = end_vertex

            cuts = obj.getCutVars()
            self._edges[key]={
                'id': key,
                'name': name,
                'source': start_vertex._get_ID(),
                'target': end_vertex._get_ID(),
                'Pt':cuts[0],
                'E': cuts[1],
                'Eta': cuts[2]
            }

            for d in range(0, len(daughters)):
                self._process_daughter(daughters[d], end_vertex, level+1)
           
        dummy_root = Vertex("0", 0)
        for i in range(0, len(root_IDs)):
            dummy_root._add_child(self._vertices[root_IDs[i]])
        b = buchheim(dummy_root, 'feynman')
        self._convert_positions(b)
        self._symmetrize_placement()
        
    def _process_daughter(self, key, start_vertex, level):
        if self._edges.has_key(key):
            return
        obj = self._object_list[key]
        name = obj.getName()
        relatives = obj.getPXLRelatives()

        if not relatives:
            return
        daughters = relatives[0]['Daughter ids']
        mothers = relatives[2]['Mother ids']

        end_vertex = None
        for k in self._vertices:
            if self._vertices[k]._has_in(key):
                end_vertex=self._vertices[k]
            if self._vertices[k]._has_out(key) and self._vertices[k]._get_ID() != start_vertex._get_ID():
                end_vertex=self._vertices[k]
                break

        if not end_vertex:
            self._vertex_count += 1
            end_vertex = Vertex(str(self._vertex_count), level+1)
        start_vertex._add_child(end_vertex)

        for m in range(0, len(mothers)):
            start_vertex._add_in(mothers[m])
        for m in range(0, len(daughters)):
            end_vertex._add_out(daughters[m])
        end_vertex._add_in(key) 

        self._vertices[start_vertex._get_ID()] = start_vertex
        self._vertices[end_vertex._get_ID()] = end_vertex

        cuts = obj.getCutVars()
        self._edges[key]={
            'id': key,
            'name': name,
            'source': start_vertex._get_ID(),
            'target': end_vertex._get_ID(),
            'pt':cuts[0],
            'E': cuts[1],
            'Eta': cuts[2]
        }
        for d in range(0, len(daughters)):
            self._process_daughter(daughters[d], end_vertex, level+1)
    #Step4)
    def _convert_positions(self, bheim):
        for roots in bheim.children:
            self._add_position_entry(roots, 0)
    #Step5)
    def _symmetrize_placement(self):  
        #1)find for each x-line minimal and maximal y-positions as well as the global maximum
        line_mins = {}
        line_maxs = {}
        global_x_max = 0
        for ID in self._positions:
            x = self._positions[ID]['x']
            y = self._positions[ID]['y']
            global_x_max = x if x > global_x_max else global_x_max
            if not str(y) in line_mins:
                line_mins[str(y)] = x
                line_maxs[str(y)] = x
            else:
                line_mins[str(y)] = x if x < line_mins[str(y)] else line_mins[str(y)]
                line_maxs[str(y)] = x if x > line_maxs[str(y)] else line_maxs[str(y)]
        #2)Recalculate the position by correcting for offsets and keeping relative positions
        for ID in self._positions:
            x = self._positions[ID]['x']
            y = self._positions[ID]['y']
            off = 0    #(y%2)/50.       #use the outcommented value if edgeHovering is enabled
            if (line_maxs[str(y)]-line_mins[str(y)] == 0):
                self._positions[ID]['x'] = 0.5 * global_x_max + off
            else:
                self._positions[ID]['x'] = global_x_max * 1.0 * (self._positions[ID]['x']-line_mins[str(y)])/(1.0*(line_maxs[str(y)]-line_mins[str(y)])) + off
        #3)Determine maximal positions
        self._max_x = 0
        self._max_y = 0
        for ID in self._positions:
            x = self._positions[ID]['x']
            y = self._positions[ID]['y']
            self._max_x = x if x > self._max_x else self._max_x
            self._max_y = y if y > self._max_y else self._max_y

    def _add_position_entry(self, obj, depth):
        self._positions[obj.ID] = {
            'name': obj.ID,
            'x': obj.x, 
            'y': obj.y,
            'level': obj.level
        }
        for child in obj.children:
            self._add_position_entry(child, depth+1)

    ''' Public function '''
    def get_feynman(self):
        logger.info("get the feynman graph")
        if len(self._edges) > 0:
            YByX = 1.0* self._max_y/self._max_x
        else:
            YByX = 1.
        return {
            'edges': self._edges,
            'positions': self._positions,
            'YByX':  YByX
        }