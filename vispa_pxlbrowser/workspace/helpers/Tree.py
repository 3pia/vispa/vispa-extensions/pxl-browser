import math
import time
#the algorithm for positioning
from Buchheim import *

import logging
logger = logging.getLogger(__name__)

#this class should calculate and contain all relevant data for the tree view
class Tree:
    def __init__(self, idDict):
        self._tree = []
        self._positions = {}

        self._object_list = idDict
        self._max_x = 0
        self._max_y = 0
       
        #Make the tree structure in four steps:
        #Step 1) Find the roots i.e. all objects with >0 children und no mothers
        start = time.time()
        roots = self._find_roots()
        #Step 2) Recursively place and link the objects
        for i in range(0, len(roots)):
            self._tree.append(self._make_leafs(roots[i]))
        if len(self._tree) == 0:
            return
        #Step 3) Uses Buchheim algorithm
        #See: http://billmill.org/pymag-trees/
        b = buchheim({
            'name': 'event',
            'key': '000',
            'children': self._tree,
            'cuts': (0.,0.,0.)}, "tree")
        #Step 4)
        self._convert_positions(b)
        #Step5)
        self._symmetrize_placement()
        
        end = time.time()
        logger.info("It took "+str(end-start)+" seconds for positioning")

    #Step1)
    def _find_roots(self):
        if not self._object_list:
            return
        root_objects = []
        for key in self._object_list:
            relatives = self._object_list[key].getPXLRelatives()
            if not relatives:
                continue
            daughters = relatives[0]['Daughter ids']
            mothers = relatives[2]['Mother ids']
            isRoot = len(daughters)>0 and len(mothers)==0
            if (isRoot):
                root_objects.append(key)
        return root_objects

    #Step2)
    def _make_leafs(self, key):
        daughter_keys = self.__get_daughters(key)
        daughters = []      
        for i in range(0, len(daughter_keys)):
            daughters.append(self._make_leafs(daughter_keys[i]))

        return {
            'key': key,
            'name': self.__get_name(key),
            'children': daughters,
            'cuts': self._object_list[key].getCutVars()
        }

    #Step4)
    def _convert_positions(self, bheim):
        for roots in bheim.children:
            self._add_position_entry(roots, 0)

    #Step5)
    def _symmetrize_placement(self):  
        #1)find for each x-line minimal and maximal y-positions as well as the global maximum
        line_mins = {}
        line_maxs = {}
        global_x_max = 0
        for ID in self._positions:
            x = self._positions[ID]['x']
            y = self._positions[ID]['y']
            global_x_max = x if x > global_x_max else global_x_max

            if not str(y) in line_mins:
                line_mins[str(y)] = x
                line_maxs[str(y)] = x
            else:
                line_mins[str(y)] = x if x < line_mins[str(y)] else line_mins[str(y)]
                line_maxs[str(y)] = x if x > line_maxs[str(y)] else line_maxs[str(y)]
        #2)Recalculate the position by correcting for offsets and keeping relative positions
        for ID in self._positions:
            x = self._positions[ID]['x']
            y = self._positions[ID]['y']
            if (line_maxs[str(y)]-line_mins[str(y)] == 0):
                self._positions[ID]['x'] = 0.5 * global_x_max 
            else:
                self._positions[ID]['x'] = global_x_max * 1.0 * (self._positions[ID]['x']-line_mins[str(y)])/(1.0*(line_maxs[str(y)]-line_mins[str(y)]))
        #3)Determine a mean distance sqrt(s_0) for further calculations
        self._max_x = 0
        self._max_y = 0
        for ID in self._positions:
            x = self._positions[ID]['x']
            y = self._positions[ID]['y']
            self._max_x = x if x > self._max_x else self._max_x
            self._max_y = y if y > self._max_y else self._max_y


    def _add_position_entry(self, obj, depth):
        self._positions[obj.key] = {
            'name': obj.name,
            'x': obj.x, 
            'y': obj.y,
            'Pt': obj.cuts[0],
            'E': obj.cuts[1],
            'Eta': obj.cuts[2]
        }
        for child in obj.children:
            self._add_position_entry(child, depth+1)

    ''' Helper functions '''
    def __get_mothers(self, key):
        return self._object_list[key].getPXLRelatives()[2]['Mother ids']

    def __get_daughters(self, key):
        return self._object_list[key].getPXLRelatives()[0]['Daughter ids']

    def __get_name(self, key):
        return self._object_list[key].getName()

    ''' Public function '''
    def get_tree(self):
        logger.info("get the tree structure")
        if len(self._tree) > 0:
            YByX = 1.0* self._max_y/self._max_x
        else:
            YByX = 1.
        return {
            'tree': self._tree,
            'positions': self._positions,
            'YByX':  YByX
        }