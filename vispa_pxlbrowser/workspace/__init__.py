import os
import json
import vispa
import threading

import logging
logger = logging.getLogger(__name__)

#some reader imports
from pxl import core
from DataCache import *
from DataAccessor import *

def expand(path):
    return os.path.expanduser(os.path.expandvars(path))

# define a class that implements arbitrary code
class PXLBrowserRpc:
    def __init__(self, window_id, view_id):
        logger.debug("PXLBrowserRpc instance created")
        self._window_id = window_id
        self._view_id = view_id
        self._topic = "extension.%s.socket" % self._view_id

        #some reader parameters
        self._filepath = None  #path to the file
        self._index = None   #current event number
        self._file = None   #actual pxl file instance
        self.cache = DataCache()  #the cache

    def send(self, topic, data=None):
        vispa.remote.send_topic(self._topic+"."+topic, window_id=self._window_id, data=data)      

    '''public functions'''
    # opens the file
    def start_reader(self, filepath):
        logger.info("Initializing PxlIOReader for file: " + filepath)
        self._filepath = expand(filepath).encode('ascii', 'ignore')
        self._index = 0    #the current page (= index) is stored in this class
        self._file = core.InputFile(self._filepath)

    # reads the event at the current fileposition
    def getOverview(self):
        logger.info("getting the cache")
        ovw = self.cache.getOverview()
        self.send('getOverview', ovw)

    #returns the relevant information for the feynman graph
    def getFeynman(self):
        logger.info("getting the feynman graph")
        feynman = self.cache.getFeynman()
        self.send('getFeynman', feynman)

    #returns the relevant information for the tree generation
    def getTree(self):
        logger.info("getting the tree")
        tree = self.cache.getTree()
        self.send('getTree', tree)

    # reads the properties of an object with the ID 'oid' in the current event
    def getObjectProperties(self, oid):
        logger.info("getting object properties of " + str(oid))
        obj_properties = self.cache.getObjectProperties(oid)
        self.send('getObjectProperties', {
            'oid': oid,
            'properties': obj_properties})

    def setFilter(self, indexed_objects, forbidden_strings):
        logger.info("setting the overview limits to "+str(indexed_objects)+"  "+str(forbidden_strings))
        self.cache.setDisplayLimits(indexed_objects, forbidden_strings)

    # Goes to the specified position in the file
    # The implementation here is hacked because
    # pxl-function '.seekToObject' is going forward through the file 
    def goto(self, index, indexed_objects, forbidden_strings):
        self.setFilter(indexed_objects, forbidden_strings)
        logger.info("going to event # " + str(index))
        index = int(index)
        
        if (index == self._index):
            success = True
        elif (index >= 1):    #minimal index for an event is one!
            #check if an event is already loaded in the cache
            if (self.cache.check_cache(index)):
                self.cache.load_from_cache(index)
                self._index = index
                success = True

            else:
                self._file.close()
                self._file = core.InputFile(self._filepath)
                self._index = self.loopEvents(index)
                self.cache.memorize_current_event(self._index)
                success = True
        else:
            success = False
        self.send("goto", {
            'newIndex': self._index,
            'success': success     
            })

    def numberOfEvents(self):
        logger.info("determining the number of events")
        NoF = 0
        estimate = False
        loop_file = core.InputFile(self._filepath)
        while (loop_file.readNextObject()):
            NoF = NoF + 1
            if (NoF) == 100:
                estimate = True
                break
        self.send('numberOfEvents', {
            'NoF': NoF,
            'estimate': estimate
            })        

    #closes the reader instance by clearing the cache and closing the file
    def close(self):
        logger.info("closing the file")
        self.cache.clear_cache()
        self.cache = None
        if (self._file):
            self._file.close()

    '''private functions'''    
    def loopEvents(self, index):
        logger.info("looping through the events")
        new_index = 0
        event = None
        
        for i in range(0, index):
            event = self._file.readNextObject()
            new_index = new_index + 1
            if not event:
                break
        self.cache.clear_event()
        self.cache.loadEventObjects(DataObject(event))
        return new_index