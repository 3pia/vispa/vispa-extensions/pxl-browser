#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


setup(
    name             = "vispa_pxl_browser",
    version          = "0.1.0",
    description      = "VISPA PXL Browser - Inspect contents of pxlio files.",
    author           = "VISPA Project",
    author_email     = "vispa@lists.rwth-aachen.de",
    url              = "http://vispa.physik.rwth-aachen.de/",
    license          = "GNU GPL v2",
    packages         = find_packages(),
    package_dir      = {"vispa_pxlbrowser": "vispa_pxlbrowser"},
    package_data     = {"vispa_pxlbrowser": [
        "workspace/*",
        "static/css/*",
        "static/js/*",
        "static/html/*",
        "static/img/*"
    ]},
    # install_requires = ["vispa"],
)
